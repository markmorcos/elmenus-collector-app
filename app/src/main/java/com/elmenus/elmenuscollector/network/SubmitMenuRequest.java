package com.elmenus.elmenuscollector.network;

import org.json.JSONException;

public class SubmitMenuRequest extends WebServiceRequest {
    private static final String SERVICE_ENDPOINT = "menu/create";
    public static final String PARAM_RESTAURANT_ID = "restaurant_id";
    public static final String PARAM_TITLE_EN = "title_en";
    public static final String PARAM_TITLE_AR = "title_ar";

    @Override
    public String getQueryString() {
        try {
            generatePostJSONObject(SERVICE_ENDPOINT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return BASE_URL + SERVICE_ENDPOINT;
    }
}