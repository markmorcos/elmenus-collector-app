package com.elmenus.elmenuscollector.network;

import org.json.JSONException;

public class SubmitContactRequest extends WebServiceRequest {
    private static final String SERVICE_ENDPOINT = "admin/insertContactApi";
    public static final String PARAM_RESTAURANT_ID = "restaurantID";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_MOBILE_NUMBER = "number";
    public static final String PARAM_JOB_TITLE = "position";
    public static final String PARAM_NOTES = "notes";

    @Override
    public String getQueryString() {
        try {
            generatePostJSONObject(SERVICE_ENDPOINT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return BASE_URL + SERVICE_ENDPOINT;
    }
}