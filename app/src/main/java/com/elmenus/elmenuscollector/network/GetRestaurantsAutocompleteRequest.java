package com.elmenus.elmenuscollector.network;

public class GetRestaurantsAutocompleteRequest extends WebServiceRequest {
    private static final String SERVICE_NAME = "restaurants/autoComplete";
    public static final String PARAM_QUERY = "q";
    public static final String PARAM_CITY_ID = "cityID";
    public static final String PARAM_OFFSET = "offset";
    public static final String PARAM_LIMIT = "limit";
    public static final String PARAM_LATITUDE = "lat";
    public static final String PARAM_LONGITUDE = "lon";

    @Override
    public String getQueryString() {
        return BASE_URL + SERVICE_NAME + generateGetQueryString(SERVICE_NAME);
    }
}