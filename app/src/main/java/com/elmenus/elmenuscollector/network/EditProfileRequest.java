package com.elmenus.elmenuscollector.network;

import org.json.JSONException;

public class EditProfileRequest extends WebServiceRequest {
    private static final String SERVICE_NAME = "users/%s/profile";
    private String serviceName;
    public static final String PARAM_NAME = "name";
    public static final String PARAM_PASSWORD = "password";

    public EditProfileRequest(long userId) {
        serviceName = String.format(SERVICE_NAME, Long.toString(userId));
    }

    @Override
    public String getQueryString() {
        try {
            generatePostJSONObject(serviceName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return BASE_URL + serviceName;
    }
}