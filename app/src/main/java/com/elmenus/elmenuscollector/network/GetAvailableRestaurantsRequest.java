package com.elmenus.elmenuscollector.network;

public class GetAvailableRestaurantsRequest extends WebServiceRequest {
    private static final String SERVICE_NAME = "restaurants/availableForCollection";

    @Override
    public String getQueryString() {
        return BASE_URL + SERVICE_NAME + generateGetQueryString(SERVICE_NAME);
    }
}