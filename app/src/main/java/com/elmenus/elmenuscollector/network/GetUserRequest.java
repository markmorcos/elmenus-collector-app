package com.elmenus.elmenuscollector.network;

public class GetUserRequest extends WebServiceRequest {
    private static final String SERVICE_NAME = "users/%s";
    private String serviceName;

    public GetUserRequest(long userId) {
        serviceName = String.format(SERVICE_NAME,Long.toString(userId));
    }

    @Override
    public String getQueryString() {
        return BASE_URL + serviceName + generateGetQueryString(serviceName);
    }
}