package com.elmenus.elmenuscollector.network;

public class GetFeaturesRequest extends WebServiceRequest {
    private static final String SERVICE_NAME = "features";

    @Override
    public String getQueryString() {
        return BASE_URL + SERVICE_NAME + generateGetQueryString(SERVICE_NAME);
    }
}