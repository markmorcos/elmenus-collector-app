package com.elmenus.elmenuscollector.network;

public class GetAreasRequest extends WebServiceRequest {
    private static final String SERVICE_NAME = "areas";

    @Override
    public String getQueryString() {
        return BASE_URL + SERVICE_NAME + generateGetQueryString(SERVICE_NAME);
    }
}