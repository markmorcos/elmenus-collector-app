package com.elmenus.elmenuscollector.network;

public class GetCitiesRequest extends WebServiceRequest {
    private static final String SERVICE_NAME = "cities";

    @Override
    public String getQueryString() {
        return BASE_URL + SERVICE_NAME + generateGetQueryString(SERVICE_NAME);
    }
}