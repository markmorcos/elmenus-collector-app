package com.elmenus.elmenuscollector.network;

public class GetCityRequest extends WebServiceRequest {
    private static final String SERVICE_NAME = "cities/%s";
    private String serviceName;

    public GetCityRequest(long cityId) {
        serviceName = String.format(SERVICE_NAME, Long.toString(cityId));
    }

    @Override
    public String getQueryString() {
        return BASE_URL + serviceName + generateGetQueryString(SERVICE_NAME);
    }
}