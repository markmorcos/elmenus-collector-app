package com.elmenus.elmenuscollector.network;

public class GetPaymentSummaryRequest extends WebServiceRequest {
    private static final String SERVICE_NAME = "users/viewPaymentSummary";
    public static final String PARAM_USER_ID = "userID";

    @Override
    public String getQueryString() {
        return BASE_URL + SERVICE_NAME + generateGetQueryString(SERVICE_NAME);
    }
}