package com.elmenus.elmenuscollector.network;

public class GetCuisinesRequest extends WebServiceRequest {
    private static final String SERVICE_NAME = "cuisines";

    @Override
    public String getQueryString() {
        return BASE_URL + SERVICE_NAME + generateGetQueryString(SERVICE_NAME);
    }
}