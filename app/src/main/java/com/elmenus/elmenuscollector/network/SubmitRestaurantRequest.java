package com.elmenus.elmenuscollector.network;

import org.json.JSONException;

public class SubmitRestaurantRequest extends WebServiceRequest {
    private static final String SERVICE_ENDPOINT = "restaurants/add";
    public static final String PARAM_CITY_ID = "city";
    public static final String PARAM_CUISINES = "cuisines"; // comma separated
    public static final String PARAM_DELIVERY_AREAS = "deliveryAreas"; // comma separated
    public static final String PARAM_CLASSES = "classes"; // comma separated
    public static final String PARAM_NAME_EN = "name";
    public static final String PARAM_NAME_AR = "name_ar";
    public static final String PARAM_SHORT_NUMBER = "shortNumber";
    public static final String PARAM_LOGO = "logo";
    public static final String PARAM_PICKUP_SOURCE = "pickupSource";
    public static final String PARAM_EDIT_RESTAURANT = "edit_restaurant";

    @Override
    public String getQueryString() {
        try {
            generatePostJSONObject(SERVICE_ENDPOINT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return BASE_URL + SERVICE_ENDPOINT;
    }
}