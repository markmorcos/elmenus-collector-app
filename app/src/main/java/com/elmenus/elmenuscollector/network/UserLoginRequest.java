package com.elmenus.elmenuscollector.network;

import org.json.JSONException;

public class UserLoginRequest extends WebServiceRequest {
    private static final String SERVICE_ENDPOINT = "users/login";
    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_PASSWORD = "password";

    @Override
    public String getQueryString() {
        try {
            generatePostJSONObject(SERVICE_ENDPOINT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return BASE_URL + SERVICE_ENDPOINT;
    }
}