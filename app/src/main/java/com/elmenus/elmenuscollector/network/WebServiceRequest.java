package com.elmenus.elmenuscollector.network;

import android.content.Context;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;

import com.elmenus.elmenuscollector.helpers.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.LinkedHashMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public abstract class WebServiceRequest {
    protected final static String BASE_URL = "http://staging2.api.elmenus.com:3000/v1/";
    private final static String API_APP_ID = "51c7335ded44daf25d000006";
    private final static String API_SECRET = "c99fd8928i2yu5gghher43578cvk65llk14453w582e";
    /*
    protected final static String BASE_URL = "http://staging2.api.elmenus.com:3000/v1/";
    private final static String API_APP_ID = "51c7335ded44daf25d000006";
    private final static String API_SECRET = "c99ff8928i2qk5ltyher43578cvk65llk12srqw582e"
    protected final static String BASE_URL = "http://api.elmenus.com/v1/";
    private final static String API_APP_ID = "51c7335ded44daf25d000007";
    private final static String API_SECRET = "c99fd8d28igyu5ga3hxr435t8cvk659lk12drvwy82e";
    */

    protected LinkedHashMap serviceParameters = new LinkedHashMap();
    public abstract String getQueryString();
    private Context context;
    private JSONObject jsonRequest;

    public static final String PARAM_LONGITUDE = "lon";
    public static final String PARAM_LATITUDE = "lat";
    public static final String PARAM_LIMIT = "limit";
    public static final String PARAM_OFFSET = "offset";

    public JSONObject getJsonRequest() {
        return jsonRequest;
    }

    public void setJsonRequest(JSONObject jsonRequest) {
        this.jsonRequest = jsonRequest;
    }

    public void setContext(Context c){
        context = c;
    }
    public Context getContext(){
        return context;
    }

    public WebServiceRequest addParameter(String key, String value){
        if(serviceParameters == null){
            serviceParameters = new LinkedHashMap();
        }
        serviceParameters.put(key, value);
        return this;
    }

    private Uri.Builder createURIBuilder(String serviceName){
        if(context == null) {
            //throw new IllegalArgumentException("Context not set for request. Please make sure you call request.setContext() before generating request");
            Log.d("elmenus WebServiceRequest Error","Context not set for request or is null. Please make sure you call request.setContext() before generating request");
        }

        Uri.Builder builder = new Uri.Builder();

        if(serviceParameters!=null){
            Iterator keys = serviceParameters.keySet().iterator();
            while(keys.hasNext()){
                String paramName = (String)keys.next();
                String paramValue = (String)serviceParameters.get(paramName);
                builder.appendQueryParameter(paramName, paramValue);
            }
        }

        try{
            String accessToken = Constants.accessToken;
            if (accessToken == null) {
                accessToken = "";
            }
            if(accessToken.length()>0)
                builder.appendQueryParameter("userAccessToken", accessToken);
        }catch (NullPointerException npe){

        }

        return builder;
    }

//    protected void generatePostJSONObject(String serviceName) throws JSONException{
//        String method = "POST";
//        serviceParameters.put("nonce",Long.toString(System.currentTimeMillis()));
//        serviceParameters.put("appID",API_APP_ID);
//        String accessToken = ElMenusPreferences.getPrefs(context).getString(ElMenusPreferences.KEY_USER_ACCESSTOKEN, "");
//        if(accessToken!=null)
//            serviceParameters.put("userAccessToken",accessToken);
//        Iterator<Map.Entry> iterator = serviceParameters.entrySet().iterator();
//        Uri.Builder build = new Uri.Builder();
//        while(iterator.hasNext()){
//            Map.Entry entry = iterator.next();
//            build.appendQueryParameter((String)entry.getKey(),(String)entry.getValue());
//        }
//        Log.d("Service Parameters",serviceParameters.toString());
//        serviceParameters.put("signature",signRequest(method,serviceName,build.toString().substring(1)));
//        jsonRequest = new JSONObject(convertMapToJsonString()) ;
//    }
//
//    private String convertMapToJsonString(){
//        StringBuilder sb = new StringBuilder();
//        Iterator<Map.Entry> entries = serviceParameters.entrySet().iterator();
//        sb.append("{");
//        while(entries.hasNext()){
//            Map.Entry entry = entries.next();
//            if(entries.hasNext())
//                sb.append("\""+entry.getKey()+"\":\""+entry.getValue()+"\",");
//            else
//                sb.append("\""+entry.getKey()+"\":\""+entry.getValue()+"\"");
//        }
//        sb.append("}");
//
//        return sb.toString();
//    }

    protected void generatePostJSONObject(String serviceName) throws JSONException {
        String method = "POST";
        jsonRequest = new JSONObject();

        serviceParameters.put("nonce",Long.toString(System.currentTimeMillis()));
        serviceParameters.put("appID",API_APP_ID);
        Iterator keys = serviceParameters.keySet().iterator();

        while(keys.hasNext()){
            String paramName = (String)keys.next();
            String paramValue = (String)serviceParameters.get(paramName);
            jsonRequest.put(paramName,paramValue);
        }

        String accessToken = Constants.accessToken;
        if (accessToken == null) {
            accessToken = "";
        }
        if(accessToken.length()>0) {
            jsonRequest.put("userAccessToken", accessToken);
        }

        //jsonRequest.put("signature",signRequest(method,serviceName,builder.toString().substring(1)));
        jsonRequest.put("signature","");
        String signature = signJsonRequest(method,serviceName,jsonRequest);
        jsonRequest.put("signature", signature);

        //return jsonRequest;
    }

    protected String generateGetQueryString(String serviceName){
        String request = "";
        String method = "GET";
        serviceParameters.put("nonce",Long.toString(System.currentTimeMillis()));
        serviceParameters.put("appID",API_APP_ID);
        Uri.Builder builder = createURIBuilder(serviceName);
        builder.appendQueryParameter("signature",signRequest(method, serviceName,builder.toString().substring(1)));
        return builder.toString();
    }

    private String signJsonRequest(String method, String serviceName, JSONObject jsonReq){
        String signature = "";

        Iterator<String> keys = jsonReq.keys();
        Uri.Builder builder = new Uri.Builder();
        while(keys.hasNext()){
            String key = keys.next();
            if(key.equals("signature"))
                continue;
            try {
                String value = jsonReq.getString(key);
                builder.appendQueryParameter(key,value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return signRequest(method,serviceName,builder.toString().substring(1));
    }

    private String signRequest(String method, String serviceName, String normalizedParams){

        String url = "/v1/"+serviceName;
        String requestString = method+"&"+url+"&"+normalizedParams;


        String signature = null;
        try {
            signature = sha1(requestString,API_SECRET);
            //Log.d("Signing Request",signature);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return signature;
    }

    private static String sha1(String s, String keyString) throws
            UnsupportedEncodingException, NoSuchAlgorithmException,
            InvalidKeyException {
        Mac mac = Mac.getInstance("HmacSHA1");
        SecretKeySpec secret = new SecretKeySpec(keyString.getBytes("UTF8"), mac.getAlgorithm());
        mac.init(secret);
        byte[] digest = mac.doFinal(s.getBytes("UTF8"));
        String signature = Base64.encodeToString(digest, Base64.NO_WRAP);
        return signature;
    }
}