package com.elmenus.elmenuscollector.network;

import org.json.JSONException;

public class SubmitBranchRequest extends WebServiceRequest {
    private static final String SERVICE_ENDPOINT = "branches/create";
    public static final String PARAM_RESTAURANT_ID = "restaurantID";
    public static final String PARAM_ADDRESS_EN = "address";
    public static final String PARAM_ADDRESS_AR = "address_ar";
    public static final String PARAM_LONGITUDE = "longitude";
    public static final String PARAM_LATITUDE = "latitude";
    public static final String PARAM_AREA = "area";
    public static final String PARAM_FEATURES = "features";
    public static final String PARAM_PHONE_NUMBER = "phoneNumbers";
    public static final String PARAM_OPENING_HOURS = "workingHours";

    @Override
    public String getQueryString() {
        try {
            generatePostJSONObject(SERVICE_ENDPOINT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return BASE_URL + SERVICE_ENDPOINT;
    }
}