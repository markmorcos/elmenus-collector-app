package com.elmenus.elmenuscollector.network;

import org.json.JSONException;

public class SubmitBranchImageRequest extends WebServiceRequest {
    private static final String SERVICE_NAME = "branch/%s/addScannedImage";
    private String serviceName;
    public static final String PARAM_USER_ID = "user_id";
    public static final String PARAM_FILE = "file";
    public static final String PARAM_REAL_NAME = "realName";

    public SubmitBranchImageRequest(long branchId) {
        serviceName = String.format(SERVICE_NAME, Long.toString(branchId));
    }

    @Override
    public String getQueryString() {
        try {
            generatePostJSONObject(serviceName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return BASE_URL + serviceName;
    }
}