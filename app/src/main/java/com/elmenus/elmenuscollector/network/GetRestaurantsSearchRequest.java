package com.elmenus.elmenuscollector.network;

public class GetRestaurantsSearchRequest extends WebServiceRequest {
    private static final String SERVICE_NAME = "restaurants/search";
    public static final String PARAM_QUERY = "q";
    public static final String PARAM_CITY_ID = "cityID";
    public static final String PARAM_OFFSET = "offset";
    public static final String PARAM_LIMIT = "limit";
    public static final String PARAM_LATITUDE = "lat";
    public static final String PARAM_LONGITUDE = "lon";
    public static final String PARAM_SORT = "sort";
    public static final String PARAM_ASSIGNED = "collection_assigned";
    public static final String PARAM_COLLECTED = "collection_collected";
    public static final String PARAM_STATUS = "collection_status";

    @Override
    public String getQueryString() {
        return BASE_URL + SERVICE_NAME + generateGetQueryString(SERVICE_NAME);
    }
}