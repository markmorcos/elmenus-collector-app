package com.elmenus.elmenuscollector.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.elmenus.elmenuscollector.R;
import com.elmenus.elmenuscollector.models.Model;

import java.util.ArrayList;

public class ModelSpinnerAdapter extends ArrayAdapter<Model> {

    ArrayList<Model> items;
    Context context;
    int resource;

    static class Holder {
        TextView text;
    }

    public ModelSpinnerAdapter(Context context, int resource, ArrayList<Model> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.items = objects;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final Holder holder;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            holder = new Holder();
            holder.text = (TextView) row.findViewById(R.id.spinner_text);
            row.setTag(holder);
        } else {
            holder = (Holder) row.getTag();
        }
        holder.text.setText(items.get(position).toString());
        return row;
    }
}
