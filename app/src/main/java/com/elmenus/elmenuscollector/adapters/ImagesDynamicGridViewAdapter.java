package com.elmenus.elmenuscollector.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.elmenus.elmenuscollector.activities.BaseActivity;
import com.elmenus.elmenuscollector.helpers.ImageItem;
import com.elmenus.elmenuscollector.R;

import org.askerov.dynamicgrid.BaseDynamicGridAdapter;
import org.askerov.dynamicgrid.DynamicGridUtils;

import java.util.ArrayList;

public class ImagesDynamicGridViewAdapter extends BaseDynamicGridAdapter {

    Context context;
    int resource;
    ArrayList<ImageItem> items;

    static class Holder {
        ImageView thumbnail, rotate;
    }

    @Override
    public void reorderItems(int originalPosition, int newPosition) {
        super.reorderItems(originalPosition, newPosition);
        if (newPosition < getCount()) {
            DynamicGridUtils.reorder(items, originalPosition, newPosition);
        }
    }

    public ImagesDynamicGridViewAdapter(Context context, int resource, ArrayList<ImageItem> items) {
        super(context, items, 3);
        this.context = context;
        this.resource = resource;
        this.items = items;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final Holder holder;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            holder = new Holder();
            holder.thumbnail = (ImageView) row.findViewById(R.id.thumbnail);
            holder.rotate = (ImageView) row.findViewById(R.id.rotate);
            row.setTag(holder);
        } else {
            holder = (Holder) row.getTag();
        }
        final ImageItem item = items.get(position);
        holder.thumbnail.setImageBitmap(BaseActivity.rotateBitmap(item.getBitmap(), item.getRotation()));
        holder.rotate.setTag(R.drawable.ic_menu_rotate);
        holder.rotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item.setRotation((item.getRotation() + 90) % 360);
                holder.thumbnail.setImageBitmap(BaseActivity.rotateBitmap(item.getBitmap(), item.getRotation()));
            }
        });
        return row;
    }

    public void remove(ImageItem item) {
        items.remove(item);
        super.remove(item);
    }
}
