package com.elmenus.elmenuscollector.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.elmenus.elmenuscollector.R;
import com.elmenus.elmenuscollector.activities.MyCollectionsActivity;
import com.elmenus.elmenuscollector.helpers.Constants;
import com.elmenus.elmenuscollector.models.Restaurant;

import java.util.ArrayList;
import java.util.Collection;

public class AvailableRestaurantsListViewAdapter extends ArrayAdapter<Restaurant> {

    ArrayList<Restaurant> items;
    ArrayList<Boolean> checked;
    Context context;
    int resource;

    static class Holder {
        CheckBox checkbox;
        TextView text;
        Button collect;
    }

    public AvailableRestaurantsListViewAdapter(Context context, int resource, ArrayList<Restaurant> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.items = objects;
        checked = new ArrayList<>();
        for (int i = 0; i < items.size(); ++i) {
            checked.add(false);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final Holder holder;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            holder = new Holder();
            holder.checkbox = (CheckBox) row.findViewById(R.id.checkbox);
            holder.text = (TextView) row.findViewById(R.id.text);
            holder.collect = (Button) row.findViewById(R.id.button_collect);
            row.setTag(holder);
        } else {
            holder = (Holder) row.getTag();
        }
        if (items.get(position).getAssigned() == Constants.currentUser.getId() || items.get(position).getAssigned() == 0) {
            holder.collect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, MyCollectionsActivity.class);
                    intent.setAction("edit");
                    items.get(position).setStatus("started");
                    intent.putExtra("result", (Parcelable) items.get(position));
                    context.startActivity(intent);
                    ((Activity) context).finish();
                }
            });
        } else {
            holder.collect.setVisibility(View.GONE);
        }
        holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checked.set(position, isChecked);
            }
        });
        holder.checkbox.setChecked(checked.get(position));
        holder.text.setText(items.get(position).getNameEn());
        return row;
    }

    public ArrayList<Restaurant> getSelectedItems() {
        ArrayList<Restaurant> result = new ArrayList<>();
        for(int i = 0; i < items.size(); ++i) {
            if (checked.get(i)) {
                result.add(items.get(i));
            }
        }
        return result;
    }

    @Override
    public Restaurant getItem(int position) {
        return items.get(position);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public void addAll(Collection<? extends Restaurant> collection) {
        super.addAll(collection);
        for (int i = 0; i < collection.size(); ++i) {
            checked.add(false);
        }
    }

    @Override
    public void add(Restaurant object) {
        super.add(object);
        checked.add(false);
    }
}
