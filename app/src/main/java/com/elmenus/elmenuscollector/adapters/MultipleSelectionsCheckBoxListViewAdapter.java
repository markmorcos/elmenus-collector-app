package com.elmenus.elmenuscollector.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.elmenus.elmenuscollector.R;
import com.elmenus.elmenuscollector.models.Model;

import java.util.ArrayList;

public class MultipleSelectionsCheckBoxListViewAdapter extends ArrayAdapter<Model> implements Filterable {

    ArrayList<Model> items, allItems;
    ArrayList<Boolean> checked, allChecked;
    Context context;
    int resource;
    ItemsFilter filter;

    static class Holder {
        CheckBox checkbox;
        TextView text;
    }

    public MultipleSelectionsCheckBoxListViewAdapter(Context context, int resource, ArrayList<Model> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.items = objects;
        checked = new ArrayList<>();
        allChecked = new ArrayList<>();
        ArrayList<Model> result = ((Activity) context).getIntent().getParcelableArrayListExtra("result");
        if (result == null) {
            result = new ArrayList<>();
        }
        for (int i = 0; i < items.size(); ++i) {
            boolean found = false;
            for (int j = 0; !found && j < result.size(); ++j) {
                if (items.get(i).equals(result.get(j))) {
                    found = true;
                }
            }
            checked.add(found);
            allChecked.add(found);
        }
        allItems = new ArrayList<>();
        allItems.addAll(items);
        getFilter();
        resetData();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final Holder holder;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            holder = new Holder();
            row.setTag(holder);
        } else {
            holder = (Holder) row.getTag();
        }
        holder.checkbox = (CheckBox) row.findViewById(R.id.checkBox);
        holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checked.set(position, isChecked);
            }
        });
        holder.checkbox.setChecked(checked.get(position));
        holder.text = (TextView) row.findViewById(R.id.text);
        holder.text.setText(items.get(position).toString());
        return row;
    }

    private class ItemsFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if(constraint.toString().length() > 0) {
                ArrayList<Model> filteredItems = new ArrayList<>();
                ArrayList<Boolean> checkedItems = new ArrayList<>();
                for(int i = 0, l = items.size(); i < l; i++) {
                    Model m = items.get(i);
                    if(m.toString().toLowerCase().contains(constraint)) {
                        filteredItems.add(m);
                        checkedItems.add(checked.get(i));
                    }
                }
                result.values = new Pair<>(filteredItems, checkedItems);
                result.count = filteredItems.size();
            } else {
                synchronized(this) {
                    result.values = new Pair<>(items, checked);
                    result.count = items.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            items = (ArrayList) ((Pair) results.values).first;
            checked = (ArrayList) ((Pair) results.values).second;
            notifyDataSetChanged();
            clear();
            for(int i = 0, l = items.size(); i < l; i++) {
                add(items.get(i));
            }
            notifyDataSetInvalidated();
        }
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new ItemsFilter();
        }
        return filter;
    }

    public void resetData() {
        items = allItems;
        checked = allChecked;
        notifyDataSetChanged();
        clear();
        for(int i = 0, l = items.size(); i < l; i++) {
            add(items.get(i));
        }
        notifyDataSetInvalidated();
    }

    public ArrayList<Model> getSelectedItems() {
        ArrayList<Model> result = new ArrayList<>();
        for(int i = 0; i < allItems.size(); ++i) {
            if (checked.get(i)) {
                result.add(allItems.get(i));
            }
        }
        return result;
    }

    @Override
    public Model getItem(int position) {
        return items.get(position);
    }

    @Override
    public int getCount() {
        return items.size();
    }
}
