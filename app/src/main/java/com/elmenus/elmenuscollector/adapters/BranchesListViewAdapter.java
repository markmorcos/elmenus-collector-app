package com.elmenus.elmenuscollector.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.andexert.expandablelayout.library.ExpandableLayoutItem;
import com.elmenus.elmenuscollector.R;
import com.elmenus.elmenuscollector.activities.NewBranchActivity;
import com.elmenus.elmenuscollector.activities.NewRestaurantActivity;
import com.elmenus.elmenuscollector.models.Branch;

import java.util.ArrayList;

public class BranchesListViewAdapter extends ArrayAdapter<Branch> {

    ArrayList<Branch> items;
    Context context;
    int resource;

    static class Holder {
        ExpandableLayoutItem item;
        TextView text;
        Button edit;
        TextView address;
    }

    public BranchesListViewAdapter(Context context, int resource, ArrayList<Branch> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.items = objects;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final Holder holder;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            holder = new Holder();
            holder.item = (ExpandableLayoutItem) row.findViewById(R.id.expandable_layout);
            holder.text = (TextView) holder.item.getHeaderRelativeLayout().findViewById(R.id.text);
            holder.edit = (Button) holder.item.getHeaderRelativeLayout().findViewById(R.id.button_edit);
            holder.address = (TextView) holder.item.getContentRelativeLayout().findViewById(R.id.address);
            row.setTag(holder);
        } else {
            holder = (Holder) row.getTag();
        }
        holder.text.setText(items.get(position).toString());
        holder.item.showNow();
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, NewBranchActivity.class);
                intent.setAction("edit");
                intent.putExtra("position", position);
                intent.putExtra("result", (Parcelable) items.get(position));
                ((Activity) context).startActivityForResult(intent, NewRestaurantActivity.REQUEST_CODE_BRANCH);
            }
        });
        holder.address.setText(items.get(position).getAddressEn());
        return row;
    }
}
