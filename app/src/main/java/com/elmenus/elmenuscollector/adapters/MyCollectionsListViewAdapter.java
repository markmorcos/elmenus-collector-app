package com.elmenus.elmenuscollector.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.elmenus.elmenuscollector.R;
import com.elmenus.elmenuscollector.activities.BaseActivity;
import com.elmenus.elmenuscollector.activities.MyCollectionsActivity;
import com.elmenus.elmenuscollector.activities.NewRestaurantActivity;
import com.elmenus.elmenuscollector.helpers.Constants;
import com.elmenus.elmenuscollector.models.Restaurant;

import java.util.ArrayList;

public class MyCollectionsListViewAdapter extends ArrayAdapter<Restaurant> {

    ArrayList<Restaurant> items;
    Context context;
    int resource;

    static class Holder {
        CheckBox checkbox;
        TextView text;
        Button statusButton;
        Button cancelButton;
    }

    public MyCollectionsListViewAdapter(Context context, int resource, ArrayList<Restaurant> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.items = objects;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final Holder holder;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            holder = new Holder();
            holder.checkbox = (CheckBox) row.findViewById(R.id.checkbox);
            holder.text = (TextView) row.findViewById(R.id.text);
            holder.statusButton = (Button) row.findViewById(R.id.button_status);
            holder.cancelButton = (Button) row.findViewById(R.id.button_cancel);
            row.setTag(holder);
        } else {
            holder = (Holder) row.getTag();
        }
        holder.checkbox.setVisibility(View.GONE);
        holder.statusButton.setText(BaseActivity.capitalise(items.get(position).getStatus()));
        if (!items.get(position).getStatus().equals("started") && !items.get(position).getStatus().equals("rejected")) {
            holder.statusButton.setEnabled(false);
            holder.cancelButton.setVisibility(View.GONE);
        } else {
            holder.text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.statusButton.performClick();
                }
            });
            holder.statusButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (items.get(position).getStatus().equals("rejected")) {
                        new AlertDialog.Builder(context).setTitle("Rejection Reason")
                                .setMessage(items.get(position).getRejectionReason())
                                .setNeutralButton("OK", null)
                                .create().show();
                    } else {
                        Intent intent = new Intent(context, NewRestaurantActivity.class);
                        intent.setAction("edit");
                        intent.putExtra("position", position);
                        intent.putExtra("result", (Parcelable) items.get(position));
                        ((Activity) context).startActivityForResult(intent, MyCollectionsActivity.REQUEST_CODE_RESTAURANT);
                    }
                }
            });
            holder.cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AlertDialog.Builder(context)
                            .setMessage("Are you sure you want to delete this item?")
                            .setNegativeButton("No", null)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ((MyCollectionsActivity) context).temp.remove(Constants.collections.get(position));
                                    Constants.collections.remove(position);
                                    ((BaseActivity) context).saveCollections();
                                    notifyDataSetChanged();
                                }
                            }).create().show();
                }
            });
        }
        holder.text.setText(items.get(position).getNameEn());
        return row;
    }
}
