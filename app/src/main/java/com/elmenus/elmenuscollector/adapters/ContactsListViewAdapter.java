package com.elmenus.elmenuscollector.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.elmenus.elmenuscollector.R;
import com.elmenus.elmenuscollector.activities.NewContactActivity;
import com.elmenus.elmenuscollector.activities.NewRestaurantActivity;
import com.elmenus.elmenuscollector.models.Contact;

import java.util.ArrayList;

public class ContactsListViewAdapter extends ArrayAdapter<Contact> {

    ArrayList<Contact> items;
    Context context;
    int resource;

    static class Holder {
        TextView text;
        Button edit;
    }

    public ContactsListViewAdapter(Context context, int resource, ArrayList<Contact> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.items = objects;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final Holder holder;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            holder = new Holder();
            holder.text = (TextView) row.findViewById(R.id.text);
            holder.edit = (Button) row.findViewById(R.id.button_edit);
            row.setTag(holder);
        } else {
            holder = (Holder) row.getTag();
        }
        holder.text.setText(items.get(position).toString());
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, NewContactActivity.class);
                intent.setAction("edit");
                intent.putExtra("position", position);
                intent.putExtra("result", (Parcelable) items.get(position));
                ((Activity) context).startActivityForResult(intent, NewRestaurantActivity.REQUEST_CODE_CONTACT);
            }
        });
        return row;
    }
}
