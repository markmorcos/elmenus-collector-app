package com.elmenus.elmenuscollector.models;

import android.os.Parcel;

import com.elmenus.elmenuscollector.helpers.ImageItem;
import com.elmenus.elmenuscollector.helpers.Lookups;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by anawara on 5/20/14.
 */
public class Restaurant extends Model {
    protected String nameEn, nameAr, shortNumber, descriptionEn, descriptionAr, logo, deliveryCharge,
            youtubeUrl, facebookUrl, twitterUrl, shareUrl;
    protected int favoritesCount,reviewsCount,menusCount,branchesCount,photosCount,offersCount, menuWantedCount;
    protected double overallRating;
    protected long id, createdAt;
    protected ArrayList<Branch> branches;
    protected ArrayList<Cuisine> cuisines;
    protected ArrayList<Area> deliveryAreas;
    protected String joinedCuisines, joinedDeliveryAreas;
    protected long assigned, collected;
    protected String status; //assigned, started, submitted, approved, rejected, paid
    protected String rejectionReason;
    protected boolean logoEdited;
    protected File logoFile;
    protected ArrayList<Contact> contacts;
    protected ArrayList<ImageItem> images;

    public static ArrayList<Restaurant> parseRestaurantsJsonArray(JSONArray json) {
        ArrayList<Restaurant> restaurants = new ArrayList<Restaurant>(json==null?0:json.length());
        for(int i=0; json != null && i<json.length(); i++){
            try {
                restaurants.add(Restaurant.parseRestaurantJson(json.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return restaurants;
    }


    protected static void parseIntoRestaurant(Restaurant restaurant, JSONObject json) {
        try{
            restaurant.setId(json.getLong("id"));

            JSONObject tmp = json.getJSONObject("name");
            if (tmp != null) {
                restaurant.setNameEn(tmp.getString("en"));
                restaurant.setNameAr(tmp.optString("ar"));
            }

            restaurant.setShortNumber(json.optString("shortNumber"));

            tmp = json.getJSONObject("description");
            if (tmp != null) {
                restaurant.setDescriptionEn(tmp.optString("en"));
                restaurant.setDescriptionAr(tmp.optString("ar"));
            }

            restaurant.setLogo(json.optString("logo"));
            restaurant.setDeliveryCharge(json.optString("deliveryCharge"));

            tmp = json.getJSONObject("socialLinks");
            if (tmp != null) {
                restaurant.setFacebookUrl(tmp.optString("facebook"));
                restaurant.setTwitterUrl(tmp.optString("twitter"));
                restaurant.setYoutubeUrl(tmp.optString("youtube"));
            }

            restaurant.setOverallRating(json.optDouble("overallRating"));
            restaurant.setFavoritesCount(json.optInt("favoritesCount"));
            restaurant.setReviewsCount(json.optInt("reviewsCount"));
            restaurant.setMenusCount(json.optInt("menusCount"));
            restaurant.setBranchesCount(json.optInt("branchesCount"));
            restaurant.setPhotosCount(json.optInt("photosCount"));
            restaurant.setOffersCount(json.optInt("offersCount"));

            JSONArray tmpArray = json.optJSONArray("branches");
            if(tmpArray != null){
                restaurant.setBranches(Branch.parseBranchesJsonArray(tmpArray));
            }
            restaurant.setCreatedAt(json.optLong("createdAt"));
            restaurant.setMenuWantedCount(json.optInt("menuWantedCount"));
            restaurant.setShareUrl(json.optString("shareURL"));
            tmpArray = json.optJSONArray("cuisines");
            if(tmpArray != null) {
                restaurant.setCuisines(Cuisine.parseCuisineJsonArray(tmpArray));
            }
            tmpArray = json.optJSONArray("deliveryAreas");
            if(tmpArray != null) {
                restaurant.setDeliveryAreas(Area.parseAreasJsonArray(tmpArray));
            }

             restaurant.setAssigned(json.getLong("collection_assigned"));
             restaurant.setCollected(json.getLong("collection_collected"));
             restaurant.setStatus(json.getString("collection_status"));
        } catch(JSONException je) {
            je.printStackTrace();
        }
    }

    public static Restaurant parseRestaurantJson(JSONObject json){
        Restaurant restaurant = new Restaurant();
        parseIntoRestaurant(restaurant,json);
        return restaurant;
    }

    public String getJoinedCuisines() {
        return joinedCuisines;
    }

    public void setJoinedCuisines(String joinedCuisines) {
        this.joinedCuisines = joinedCuisines;
    }

    public String getFacebookUrl() {
        return facebookUrl;
    }

    public void setFacebookUrl(String facebookUrl) {
        this.facebookUrl = facebookUrl;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }

    public String getShortNumber() {
        return shortNumber;
    }

    public void setShortNumber(String shortNumber) {
        this.shortNumber = shortNumber;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    public String getDescriptionAr() {
        return descriptionAr;
    }

    public void setDescriptionAr(String descriptionAr) {
        this.descriptionAr = descriptionAr;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(String deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public String getYoutubeUrl() {
        return youtubeUrl;
    }

    public void setYoutubeUrl(String youtubeUrl) {
        this.youtubeUrl = youtubeUrl;
    }

    public String getTwitterUrl() {
        return twitterUrl;
    }

    public void setTwitterUrl(String twitterUrl) {
        this.twitterUrl = twitterUrl;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public double getOverallRating() {
        return overallRating;
    }

    public void setOverallRating(double overallRating) {
        this.overallRating = overallRating;
    }

    public int getFavoritesCount() {
        return favoritesCount;
    }

    public void setFavoritesCount(int favoritesCount) {
        this.favoritesCount = favoritesCount;
    }

    public int getReviewsCount() {
        return reviewsCount;
    }

    public void setReviewsCount(int reviewsCount) {
        this.reviewsCount = reviewsCount;
    }

    public int getMenusCount() {
        return menusCount;
    }

    public void setMenusCount(int menusCount) {
        this.menusCount = menusCount;
    }

    public int getBranchesCount() {
        return branchesCount;
    }

    public void setBranchesCount(int branchesCount) {
        this.branchesCount = branchesCount;
    }

    public int getPhotosCount() {
        return photosCount;
    }

    public void setPhotosCount(int photosCount) {
        this.photosCount = photosCount;
    }

    public int getOffersCount() {
        return offersCount;
    }

    public void setOffersCount(int offersCount) {
        this.offersCount = offersCount;
    }

    public int getMenuWantedCount() {
        return menuWantedCount;
    }

    public void setMenuWantedCount(int menuWantedCount) {
        this.menuWantedCount = menuWantedCount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public ArrayList<Branch> getBranches() {
        return branches;
    }

    public void setBranches(ArrayList<Branch> branches) {
        this.branches = branches;
    }

    public ArrayList<Cuisine> getCuisines() {
        return cuisines;
    }

    public void setCuisines(ArrayList<Cuisine> cuisines) {
        this.cuisines = cuisines;
        StringBuilder builder = new StringBuilder();
        for(Cuisine c: cuisines){
            builder.append(c.getNameEn());
            builder.append(", ");
        }

        String tmp = builder.toString().trim();
        setJoinedCuisines((tmp.length()>0)?(tmp.substring(0,tmp.length()-1)):"");
    }

    public ArrayList<Area> getDeliveryAreas() {
        return deliveryAreas;
    }

    public void setDeliveryAreas(ArrayList<Area> deliveryAreas) {
        this.deliveryAreas = deliveryAreas;
        StringBuilder builder = new StringBuilder();

        for(Area c: deliveryAreas){
            if(Lookups.CURRENT_AREA!=null){
                if(c.getId() == Lookups.CURRENT_AREA.getId()) {
                    builder.insert(0,c.getNameEn()+", ");
                    continue;
                }
            }
            builder.append(c.getNameEn());
            builder.append(", ");
        }

        String tmp = builder.toString().trim();
        setJoinedDeliveryAreas((tmp.length()>0)?(tmp.substring(0,tmp.length()-1)):"");
    }

    public String getJoinedDeliveryAreas() {
        return joinedDeliveryAreas;
    }

    public void setJoinedDeliveryAreas(String joinedDeliveryAreas) {
        this.joinedDeliveryAreas = joinedDeliveryAreas;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nameEn);
        dest.writeString(this.nameAr);
        dest.writeString(this.shortNumber);
        dest.writeString(this.descriptionEn);
        dest.writeString(this.descriptionAr);
        dest.writeString(this.logo);
        dest.writeString(this.deliveryCharge);
        dest.writeString(this.youtubeUrl);
        dest.writeString(this.facebookUrl);
        dest.writeString(this.twitterUrl);
        dest.writeString(this.shareUrl);
        dest.writeInt(this.favoritesCount);
        dest.writeInt(this.reviewsCount);
        dest.writeInt(this.menusCount);
        dest.writeInt(this.branchesCount);
        dest.writeInt(this.photosCount);
        dest.writeInt(this.offersCount);
        dest.writeInt(this.menuWantedCount);
        dest.writeDouble(this.overallRating);
        dest.writeLong(this.id);
        dest.writeLong(this.createdAt);
        dest.writeTypedList(this.branches);
        dest.writeTypedList(this.cuisines);
        dest.writeTypedList(this.deliveryAreas);
        dest.writeString(this.joinedCuisines);
        dest.writeString(this.joinedDeliveryAreas);
        dest.writeString(this.jsonSerialization);
        dest.writeLong(this.assigned);
        dest.writeLong(this.collected);
        dest.writeString(this.status);
        dest.writeString(this.rejectionReason);
        dest.writeSerializable(logoFile);
        dest.writeInt(this.logoEdited ? 1 : 0);
        dest.writeTypedList(contacts);
        dest.writeTypedList(images);
    }

    public Restaurant() {
        nameEn = "";
        nameAr = "";
        shortNumber = "";
        descriptionEn = "";
        descriptionAr = "";
        logo = "";
        deliveryCharge = "";
        youtubeUrl = "";
        facebookUrl = "";
        twitterUrl = "";
        shareUrl = "";
        favoritesCount = 0;
        menusCount = 0;
        branchesCount = 0;
        photosCount = 0;
        offersCount = 0;
        menuWantedCount = 0;
        overallRating = 0;
        id = 0;
        createdAt = 0;
        branches = new ArrayList<>();
        cuisines = new ArrayList<>();
        deliveryAreas = new ArrayList<>();
        joinedCuisines = "";
        joinedDeliveryAreas = "";
        jsonSerialization = "";
        assigned = 0;
        collected = 0;
        status = "started";
        rejectionReason = "";
        logoFile = null;
        logoEdited = false;
        contacts = new ArrayList<>();
        images = new ArrayList<>();
    }

    private Restaurant(Parcel in) {
        this.nameEn = in.readString();
        this.nameAr = in.readString();
        this.shortNumber = in.readString();
        this.descriptionEn = in.readString();
        this.descriptionAr = in.readString();
        this.logo = in.readString();
        this.deliveryCharge = in.readString();
        this.youtubeUrl = in.readString();
        this.facebookUrl = in.readString();
        this.twitterUrl = in.readString();
        this.shareUrl = in.readString();
        this.favoritesCount = in.readInt();
        this.reviewsCount = in.readInt();
        this.menusCount = in.readInt();
        this.branchesCount = in.readInt();
        this.photosCount = in.readInt();
        this.offersCount = in.readInt();
        this.menuWantedCount = in.readInt();
        this.overallRating = in.readDouble();
        this.id = in.readLong();
        this.createdAt = in.readLong();
        this.branches = new ArrayList<Branch>();//(ArrayList<Branch>) in.readSerializable();
        in.readTypedList(branches, Branch.CREATOR);
        this.cuisines = new ArrayList<Cuisine>();
        in.readTypedList(cuisines, Cuisine.CREATOR);//(ArrayList<Cuisine>) in.readSerializable();
        this.deliveryAreas = new ArrayList<Area>();
        in.readTypedList(deliveryAreas, Area.CREATOR);//(ArrayList<Area>) in.readSerializable();
        this.joinedCuisines = in.readString();
        this.joinedDeliveryAreas = in.readString();
        this.jsonSerialization = in.readString();
        this.assigned = in.readLong();
        this.collected = in.readLong();
        this.status = in.readString();
        this.rejectionReason = in.readString();
        this.logoFile = (File) in.readSerializable();
        this.logoEdited = in.readInt() != 0;
        this.contacts = new ArrayList<>();
        in.readTypedList(contacts, Contact.CREATOR);
        this.images = new ArrayList<>();
        in.readTypedList(images, ImageItem.CREATOR);
    }
    public static final Creator<Restaurant> CREATOR = new Creator<Restaurant>() {
        public Restaurant createFromParcel(Parcel source) {
            return new Restaurant(source);
        }

        public Restaurant[] newArray(int size) {
            return new Restaurant[size];
        }
    };

    @Override
    public String toString() {
        return nameEn;
    }

    public long getAssigned() {
        return assigned;
    }

    public void setAssigned(long assigned) {
        this.assigned = assigned;
    }

    public long getCollected() {
        return collected;
    }

    public void setCollected(long collected) {
        this.collected = collected;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    public File getLogoFile() {
        return logoFile;
    }

    public void setLogoFile(File logoFile) {
        this.logoFile = logoFile;
    }

    public boolean isLogoEdited() {
        return logoEdited;
    }

    public void setLogoEdited(boolean logoEdited) {
        this.logoEdited = logoEdited;
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public ArrayList<ImageItem> getImages() {
        return images;
    }

    public void setImages(ArrayList<ImageItem> images) {
        this.images = images;
    }
}