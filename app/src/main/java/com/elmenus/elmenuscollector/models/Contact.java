package com.elmenus.elmenuscollector.models;

import android.os.Parcel;

/**
 * Created by mark on 09/07/15.
 */
public class Contact extends Model {

    private long restaurantId;
    private String name;
    private String jobTitle;
    private String mobileNumber;
    private String email;

    public Contact() {
        restaurantId = 0;
        name = "";
        jobTitle = "";
        mobileNumber = "";
        email = "";
    }

    public Contact(Parcel in) {
        restaurantId = in.readLong();
        name = in.readString();
        jobTitle = in.readString();
        mobileNumber = in.readString();
        email = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(restaurantId);
        dest.writeString(name);
        dest.writeString(jobTitle);
        dest.writeString(mobileNumber);
        dest.writeString(email);
    }

    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel source) {
            return new Contact(source);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };

    @Override
    public String toString() {
        return name;
    }

    @Override
    public long getId() {
        return 0;
    }

    public long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(long restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
