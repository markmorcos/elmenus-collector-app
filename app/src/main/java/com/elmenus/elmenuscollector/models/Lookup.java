package com.elmenus.elmenuscollector.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by anawara on 5/25/14.
 */
public class Lookup extends Model {
    protected long id;
    protected String nameEn, nameAr;
    protected boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameAr() {
        return nameAr;
    }

    public void setNameAr(String nameAr) {
        this.nameAr = nameAr;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.nameEn);
        dest.writeString(this.nameAr);
        dest.writeByte(selected ? (byte) 1 : (byte) 0);
        dest.writeString(this.jsonSerialization);
    }

    public Lookup() {
        id = 0;
        nameEn = "";
        nameAr = "";
        selected = false;
        jsonSerialization = "";
    }

    private Lookup(Parcel in) {
        this.id = in.readLong();
        this.nameEn = in.readString();
        this.nameAr = in.readString();
        this.selected = in.readByte() != 0;
        this.jsonSerialization = in.readString();
    }

    public static final Creator<Lookup> CREATOR = new Creator<Lookup>() {
        public Lookup createFromParcel(Parcel source) {
            return new Lookup(source);
        }

        public Lookup[] newArray(int size) {
            return new Lookup[size];
        }
    };

    @Override
    public String toString() {
        return nameEn;
    }
}
