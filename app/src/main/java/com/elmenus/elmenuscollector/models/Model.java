package com.elmenus.elmenuscollector.models;

import android.os.Parcelable;

import java.io.Serializable;

public abstract class Model implements Parcelable, Serializable, Comparable<Model> {
    protected String jsonSerialization;

    public Model() {
        jsonSerialization = "";
    }

    public String getJsonSerialization() {
        return jsonSerialization;
    }

    public void setJsonSerialization(String jsonSerialization) {
        this.jsonSerialization = jsonSerialization;
    }

    @Override
    public abstract String toString();

    public abstract long getId();

    @Override
    public int compareTo(Model another) {
        return getId() == another.getId() ? 0 : (getId() < another.getId() ? -1 : 1);
    }

    @Override
    public boolean equals(Object o) {
        return compareTo((Model) o) == 0;
    }
}