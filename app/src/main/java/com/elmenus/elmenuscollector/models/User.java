package com.elmenus.elmenuscollector.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class User extends Model {
    private String name, photo, birthdate, email, password, role;
    private long id, memberSince, cityId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public long getMemberSince() {
        return memberSince;
    }

    public void setMemberSince(long memberSince) {
        this.memberSince = memberSince;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getCityId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public static User parseLoginResponse(JSONObject json) {
        User user = new User();
        try {
            user.setBirthdate(json.getString("birthdate"));
            user.setId(json.getLong("id"));
            user.setMemberSince(json.getLong("memberSince"));
            user.setPhoto(json.getString("photo"));
            user.setName(json.getString("name"));
            user.setCityId(json.getLong("cityID"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return user;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.photo);
        dest.writeLong(this.id);
        dest.writeString(this.birthdate);
        dest.writeLong(this.memberSince);
        dest.writeLong(this.cityId);
        dest.writeString(this.role);
        dest.writeString(this.jsonSerialization);
    }

    public User() {
        name = "";
        photo = "";
        id = 0;
        birthdate = "";
        memberSince = 0;
        cityId = 0;
        role = "";
        jsonSerialization = "";
    }

    private User(Parcel in) {
        this.name = in.readString();
        this.photo = in.readString();
        this.id = in.readLong();
        this.birthdate = in.readString();
        this.memberSince = in.readLong();
        this.cityId = in.readLong();
        this.role = in.readString();
        this.jsonSerialization = in.readString();
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public String toString() {
        return name;
    }
}