package com.elmenus.elmenuscollector.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.elmenus.elmenuscollector.helpers.ImageItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by anawara on 5/20/14.
 */
public class Branch extends Model {

    private long id, restaurantID;
    private String phoneNumber, addressEn, addressAr, joinedFeatures;
    private Area area;
    private double longitude, latitude, distance;
    private ArrayList<OpenDetail> openDetails;
    private ArrayList<Feature> features;
    private ArrayList<ImageItem> images;

    public static String[] daysOfWeek = {"sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"};

    public static ArrayList<Branch> parseBranchesJsonArray(JSONArray jsonArray) {
        ArrayList<Branch> branches = new ArrayList<Branch>(jsonArray == null ? 0 : jsonArray.length());
        for (int i = 0; jsonArray != null && i < jsonArray.length(); i++) {
            try {
                branches.add(Branch.parseBranchJson(jsonArray.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return branches;
    }

    public static Branch parseBranchDetailsJson(JSONObject branchInfo, JSONArray features) {
        Branch branch = new Branch();
        try {

            branch.setId(branchInfo.getLong("id"));
            Area tmp = new Area();
            tmp.setNameEn(branchInfo.getString("area_name_en"));
            branch.setArea(tmp);
            branch.setAddressEn(branchInfo.optString("address"));
            JSONObject workingHours = branchInfo.getJSONObject("workingHours");
            for(int i = 0; i < 7; ++i) {
                branch.getOpenDetails().add(new OpenDetail(capitalize(daysOfWeek[i]), workingHours.getJSONObject(daysOfWeek[i])));
            }
            branch.setPhoneNumber(branchInfo.optString("phones"));
            branch.setPhoneNumber(branchInfo.optString("phoneNumbers"));
            branch.setLongitude(branchInfo.optDouble("longitude"));
            branch.setLatitude(branchInfo.optDouble("latitude"));
            branch.setDistance(branchInfo.optDouble("distance"));
            if (features != null)
                branch.setFeatures(Feature.parseFeaturesJsonArray(features));
        } catch (JSONException je) {
            je.printStackTrace();
        }
        return branch;
    }

    public static Branch parseBranchJson(JSONObject json) {
        Branch branch = new Branch();
        try {


            branch.setId(json.getLong("id"));

            JSONObject tmp = json.optJSONObject("area");
            if (tmp != null)
                branch.setArea(Area.parseAreaJson(json.getJSONObject("area")));
            else if (json.has("areaName")) {
                Area area = new Area();
                area.setNameEn(json.getString("areaName"));
                branch.setArea(area);
            }

            if (json.optString("phoneNumbers") != null)
                branch.setPhoneNumber(json.optString("phoneNumbers"));
            else if (json.has("phone_numbers"))
                branch.setPhoneNumber(json.optString("phone_numbers"));


            JSONObject location = json.optJSONObject("location");
            if (location != null) {
                branch.setLongitude(location.optDouble("lon"));
                branch.setLatitude(location.optDouble("lat"));
            } else {
                try {
                    branch.setLongitude(json.optDouble("longitude"));
                    branch.setLatitude(json.optDouble("latitude"));
                } catch (Exception je) {
                    je.printStackTrace();
                }
            }

            JSONObject address = json.optJSONObject("address");
            if (address != null) {
                branch.setAddressAr(address.optString("ar"));
                branch.setAddressEn(address.optString("en"));
            } else if (json.optString("address") != null) {
                branch.setAddressEn(json.optString("address"));
            }

            branch.setDistance(json.optDouble("distance"));
            branch.setRestaurantId(json.optLong("restaurantID"));
            branch.setFeatures(Feature.parseFeaturesJsonArray(json.optJSONArray("features")));

            JSONObject workingHours = json.getJSONObject("workingHours");
            for(int i = 0; i < 7; ++i) {
                branch.getOpenDetails().add(new OpenDetail(capitalize(daysOfWeek[i]), workingHours.getJSONObject(daysOfWeek[i])));
            }

        } catch (JSONException je) {
            je.printStackTrace();
        }
        return branch;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getRestaurantID() {
        return restaurantID;
    }

    public void setRestaurantId(long restaurantID) {
        this.restaurantID = restaurantID;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddressEn() {
        return addressEn;
    }

    public void setAddressEn(String addressEn) {
        this.addressEn = addressEn;
    }

    public String getAddressAr() {
        return addressAr;
    }

    public void setAddressAr(String addressAr) {
        this.addressAr = addressAr;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public ArrayList<OpenDetail> getOpenDetails() {
        return openDetails;
    }

    public void setOpenDetails(ArrayList<OpenDetail> openDetails) {
        this.openDetails = openDetails;
    }

    public ArrayList<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(ArrayList<Feature> features) {
        this.features = features;
        StringBuilder builder = new StringBuilder();
        for (Feature c : features) {
            builder.append(c.getNameEn());
            builder.append(", ");
        }

        String tmp = builder.toString().trim();
        setJoinedFeatures((tmp.length() > 0) ? (tmp.substring(0, tmp.length() - 1)) : "");
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getJoinedFeatures() {
        return joinedFeatures;
    }

    public void setJoinedFeatures(String joinedFeatures) {
        this.joinedFeatures = joinedFeatures;
    }

    public static class OpenDetail implements Parcelable, Serializable {
        private String dayOfWeek, from, to;

        public OpenDetail(String dayOfWeek) {
            this.dayOfWeek = dayOfWeek;
            from = "00:00";
            to = "00:00";
        }

        public OpenDetail(String dayOfWeek, JSONObject json) {
            this(dayOfWeek);
            try {
                setFrom(json.getString("from"));
                setTo(json.getString("to"));
            } catch (JSONException je) {
                je.printStackTrace();
            }
        }

        public static final Creator<OpenDetail> CREATOR = new Creator<OpenDetail>() {
            public OpenDetail createFromParcel(Parcel source) {
                return new OpenDetail(source);
            }

            public OpenDetail[] newArray(int size) {
                return new OpenDetail[size];
            }
        };

        public String getDayOfWeek() {
            return dayOfWeek;
        }

        public void setDayOfWeek(String dayOfWeek) {
            this.dayOfWeek = dayOfWeek;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.dayOfWeek);
            dest.writeString(this.from);
            dest.writeString(this.to);
        }

        private OpenDetail(Parcel in) {
            this.dayOfWeek = in.readString();
            this.from = in.readString();
            this.to = in.readString();
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeLong(this.restaurantID);
        dest.writeString(this.phoneNumber);
        dest.writeString(this.addressEn);
        dest.writeString(this.addressAr);
        dest.writeString(this.joinedFeatures);
        dest.writeParcelable(this.area, 0);
        dest.writeDouble(this.longitude);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.distance);
        dest.writeTypedList(this.openDetails);
        dest.writeTypedList(this.features);
        dest.writeTypedList(images);
        dest.writeString(this.jsonSerialization);
    }

    public Branch() {
        id = 0;
        restaurantID = 0;
        phoneNumber = "";
        addressEn = "";
        addressAr = "";
        joinedFeatures = "";
        area = new Area();
        longitude = 0;
        latitude = 0;
        distance = 0;
        openDetails = new ArrayList<>();
        features = new ArrayList<>();
        images = new ArrayList<>();
        jsonSerialization = "";
    }

    private Branch(Parcel in) {
        this.id = in.readLong();
        this.restaurantID = in.readLong();
        this.phoneNumber = in.readString();
        this.addressEn = in.readString();
        this.addressAr = in.readString();
        this.joinedFeatures = in.readString();
        this.area = in.readParcelable(Area.class.getClassLoader());
        this.longitude = in.readDouble();
        this.latitude = in.readDouble();
        this.distance = in.readDouble();
        this.openDetails = new ArrayList<OpenDetail>();
        in.readTypedList(openDetails, OpenDetail.CREATOR);
        this.features = new ArrayList<Feature>();
        in.readTypedList(features, Feature.CREATOR);//(ArrayList<Feature>) in.readSerializable();
        this.images = new ArrayList<ImageItem>();
        in.readTypedList(images, ImageItem.CREATOR);
        this.jsonSerialization = in.readString();
    }

public static final Creator<Branch> CREATOR = new Creator<Branch>() {
        public Branch createFromParcel(Parcel source) {
            return new Branch(source);
        }

        public Branch[] newArray(int size) {
            return new Branch[size];
        }
    };

    public ArrayList<ImageItem> getImages() {
        return images;
    }

    public void setImages(ArrayList<ImageItem> images) {
        this.images = images;
    }

    @Override
    public String toString() {
        return area.toString();
    }

    private static String capitalize(final String line) {
        return Character.toUpperCase(line.charAt(0)) + line.substring(1);
    }
}
