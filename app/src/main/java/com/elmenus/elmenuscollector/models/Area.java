package com.elmenus.elmenuscollector.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by anawara on 5/20/14.
 */
public class Area extends Lookup {
    private double latitude, longitude;

    public static ArrayList<Area> parseAreasJsonArray(JSONArray json){
        ArrayList<Area> areas = new ArrayList<Area>(json==null?0:json.length());
        for(int i=0; json != null && i<json.length(); i++){
            try {
                areas.add(Area.parseAreaJson(json.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return areas;
    }

    public static Area parseAreaJson(JSONObject json){
        Area area = new Area();
        try{
            area.setId(json.getLong("id"));

            JSONObject nameObject = json.optJSONObject("name");
            if(nameObject!=null){
                area.setNameAr(nameObject.optString("ar"));
                area.setNameEn(nameObject.optString("en"));
            } else{
                area.setNameAr(json.optString("name_ar"));
                area.setNameEn(json.optString("name_en"));
            }

            double center_lon = json.optDouble("center_lon");
            double center_lat = json.optDouble("center_lat");

            double lon = json.optDouble("longitude");
            double lat = json.optDouble("latitude");

            if(!Double.isNaN(center_lon))
                area.setLongitude(center_lon);
            else
                area.setLongitude(lon);

            if(!Double.isNaN(center_lat))
                area.setLatitude(center_lat);
            else
                area.setLatitude(lat);

            area.setJsonSerialization(json.toString());
        }catch(JSONException je){}

        return area;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
        dest.writeLong(this.id);
        dest.writeString(this.nameEn);
        dest.writeString(this.nameAr);
        dest.writeByte(selected ? (byte) 1 : (byte) 0);
        dest.writeString(this.jsonSerialization);
    }

    public Area() {
        latitude = 0;
        longitude = 0;
        id = 0;
        nameEn = "";
        nameAr = "";
        selected = false;
        jsonSerialization = "";
    }

    private Area(Parcel in) {
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.id = in.readLong();
        this.nameEn = in.readString();
        this.nameAr = in.readString();
        this.selected = in.readByte() != 0;
        this.jsonSerialization = in.readString();
    }

    public static final Creator<Area> CREATOR = new Creator<Area>() {
        public Area createFromParcel(Parcel source) {
            return new Area(source);
        }

        public Area[] newArray(int size) {
            return new Area[size];
        }
    };

    @Override
    public String toString() {
        return nameEn;
    }
}
