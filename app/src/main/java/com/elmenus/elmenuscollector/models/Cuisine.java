package com.elmenus.elmenuscollector.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by anawara on 5/20/14.
 */
public class Cuisine extends Lookup {
    public static Cuisine parseCuisineJson(JSONObject json){
        Cuisine cuisine = new Cuisine();
        try{
            cuisine.setId(json.getLong("id"));
            JSONObject nameObject = json.optJSONObject("name");
            if(nameObject!=null){
                cuisine.setNameEn(nameObject.getString("en"));
                cuisine.setNameAr(nameObject.optString("ar"));
            }  else{
                cuisine.setNameEn(json.optString("name_en"));
                cuisine.setNameAr(json.optString("name_ar"));
            }
        }catch(JSONException je){
            je.printStackTrace();
        }
        return cuisine;
    }

    public static ArrayList<Cuisine> parseCuisineJsonArray(JSONArray jsonArray){
        ArrayList<Cuisine> cuisines = new ArrayList<Cuisine>();
        for(int i=0; i<jsonArray.length(); i++) {
            try {
                cuisines.add(parseCuisineJson(jsonArray.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return cuisines;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.nameEn);
        dest.writeString(this.nameAr);
        dest.writeByte(selected ? (byte) 1 : (byte) 0);
        dest.writeString(this.jsonSerialization);
    }

    public Cuisine() {
        id = 0;
        nameEn = "";
        nameAr = "";
        selected = false;
        jsonSerialization = "";
    }

    private Cuisine(Parcel in) {
        this.id = in.readLong();
        this.nameEn = in.readString();
        this.nameAr = in.readString();
        this.selected = in.readByte() != 0;
        this.jsonSerialization = in.readString();
    }

    public static final Creator<Cuisine> CREATOR = new Creator<Cuisine>() {
        public Cuisine createFromParcel(Parcel source) {
            return new Cuisine(source);
        }

        public Cuisine[] newArray(int size) {
            return new Cuisine[size];
        }
    };

    @Override
    public String toString() {
        return nameEn;
    }
}
