package com.elmenus.elmenuscollector.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by anawara on 5/20/14.
 */
public class Feature extends Lookup {
    public static Feature parseFeatureJson(JSONObject json){
        Feature feature = new Feature();
        try{
            feature.setId(json.getLong("id"));
            JSONObject nameObject = json.optJSONObject("name");
            if(nameObject!=null){
                feature.setNameEn(nameObject.getString("en"));
                feature.setNameAr(nameObject.optString("ar"));
            } else if(json.has("name")){
                feature.setNameEn(json.getString("name"));
            } else{
                feature.setNameEn(json.optString("name_en"));
                feature.setNameAr(json.optString("name_ar"));
            }
        }catch(JSONException je){
            je.printStackTrace();
        }
        return feature;
    }

    public static ArrayList<Feature> parseFeaturesJsonArray(JSONArray jsonArray){
        ArrayList<Feature> features = new ArrayList<Feature>(jsonArray==null?0:jsonArray.length());
        for(int i=0; jsonArray != null && i<jsonArray.length(); i++) {
            try {
                features.add(parseFeatureJson(jsonArray.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return features;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.nameEn);
        dest.writeString(this.nameAr);
        dest.writeByte(selected ? (byte) 1 : (byte) 0);
        dest.writeString(this.jsonSerialization);
    }

    public Feature() {
        id = 0;
        nameEn = "";
        nameAr = "";
        selected = false;
        jsonSerialization = "";
    }

    private Feature(Parcel in) {
        this.id = in.readLong();
        this.nameEn = in.readString();
        this.nameAr = in.readString();
        this.selected = in.readByte() != 0;
        this.jsonSerialization = in.readString();
    }

    public static final Creator<Feature> CREATOR = new Creator<Feature>() {
        public Feature createFromParcel(Parcel source) {
            return new Feature(source);
        }

        public Feature[] newArray(int size) {
            return new Feature[size];
        }
    };

    @Override
    public String toString() {
        return nameEn;
    }
}
