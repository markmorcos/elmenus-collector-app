package com.elmenus.elmenuscollector.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.elmenus.elmenuscollector.helpers.Lookups;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by anawara on 5/20/14.
 */
public class City extends Lookup {
    private ArrayList<Area> areas;

    public static ArrayList<City> parseCitiesJson(JSONArray citiesJson){
        ArrayList<City> cities = new ArrayList<City>();

        for(int i=0; i<citiesJson.length();i++){
            try {
                City city = City.parseCityJson(citiesJson.getJSONObject(i),i);
                cities.add(city);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return cities;
    }

    public static City parseCityJson(JSONObject json, int index){
        City city = new City();

        try {
            city.setId(json.getLong("id"));
            JSONObject nameObject = json.optJSONObject("name");
            if(nameObject!=null){
                city.setNameAr(nameObject.getString("ar"));
                city.setNameEn(nameObject.getString("en"));
            } else if(json.has("name_en")){
                city.setNameAr(json.optString("name_ar"));
                city.setNameEn(json.optString("name_en"));
            }
            JSONArray jsonAreas = json.optJSONArray("areas");
            if(jsonAreas!=null){
                ArrayList<Area> areasArrayList = new ArrayList<Area>();
                for(int i=0; i<jsonAreas.length();i++){
                    Area area = Area.parseAreaJson(jsonAreas.getJSONObject(i));
                    if(Lookups.AUTODETECTED_AREA!=null)
                        if(area.getId()==Lookups.AUTODETECTED_AREA.getId()){
                            area.setSelected(true);
                            Lookups.CURRENT_AREA = Lookups.AUTODETECTED_AREA;
                            Lookups.CURRENT_CITY = city;
                            Lookups.CITY_INDEX = index;
                        }
                    areasArrayList.add(area);
                }
                if(areasArrayList.size()>0) {
                    city.setAreas(areasArrayList);
                    city.setJsonSerialization(json.toString());
                    Lookups.CITIES_MAP.put(city.getId(), city.getAreas());
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return city;
    }

    public ArrayList getAreas() {
        return areas;
    }

    public void setAreas(ArrayList areas) {
        this.areas = areas;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeSerializable(this.areas);
        dest.writeTypedList(areas);
        dest.writeLong(this.id);
        dest.writeString(this.nameEn);
        dest.writeString(this.nameAr);
        dest.writeByte(selected ? (byte) 1 : (byte) 0);
        dest.writeString(this.jsonSerialization);
    }

    public City() {
        areas = new ArrayList<>();
        id = 0;
        nameEn = "";
        nameAr = "";
        selected = false;
        jsonSerialization = "";
    }

    private City(Parcel in) {
//        this.areas = (ArrayList) in.readSerializable();
        areas = new ArrayList<Area>();
        in.readTypedList(areas,Area.CREATOR);
        this.id = in.readLong();
        this.nameEn = in.readString();
        this.nameAr = in.readString();
        this.selected = in.readByte() != 0;
        this.jsonSerialization = in.readString();
    }

    public static final Creator<City> CREATOR = new Creator<City>() {
        public City createFromParcel(Parcel source) {
            return new City(source);
        }

        public City[] newArray(int size) {
            return new City[size];
        }
    };

    @Override
    public String toString() {
        return nameEn;
    }
}
