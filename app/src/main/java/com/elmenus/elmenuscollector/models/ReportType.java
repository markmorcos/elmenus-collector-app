package com.elmenus.elmenuscollector.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by anawara on 5/28/14.
 */
public class ReportType extends Lookup {
    private int entityType;
    public static ReportType parseReportTypeJson(JSONObject json){
        ReportType report = new ReportType();
        try{
            report.setId(json.getLong("id"));
            report.setNameAr(json.optString("name_ar"));
            report.setNameEn(json.optString("name_en"));
            report.setEntityType(json.getInt("entity_id"));
        }catch(JSONException je){
            je.printStackTrace();
        }
        return report;
    }

    public static ArrayList<ReportType> parseReportTypesJsonArray(JSONArray json){
        ArrayList<ReportType> reports = new ArrayList<ReportType>(json==null?0:json.length());
        for(int i=0; json != null && i<json.length(); i++){
            try{
                reports.add(parseReportTypeJson(json.getJSONObject(i)));
            }catch (JSONException je){
                je.printStackTrace();
            }
        }
        return reports;
    }

    public int getEntityType() {
        return entityType;
    }

    public void setEntityType(int entityType) {
        this.entityType = entityType;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.entityType);
        dest.writeLong(this.id);
        dest.writeString(this.nameEn);
        dest.writeString(this.nameAr);
        dest.writeByte(selected ? (byte) 1 : (byte) 0);
        dest.writeString(this.jsonSerialization);
    }

    public ReportType() {
        entityType = 0;
        id = 0;
        nameEn = "";
        nameAr = "";
        selected = false;
        jsonSerialization = "";
    }

    private ReportType(Parcel in) {
        this.entityType = in.readInt();
        this.id = in.readLong();
        this.nameEn = in.readString();
        this.nameAr = in.readString();
        this.selected = in.readByte() != 0;
        this.jsonSerialization = in.readString();
    }

    public static final Creator<ReportType> CREATOR = new Creator<ReportType>() {
        public ReportType createFromParcel(Parcel source) {
            return new ReportType(source);
        }

        public ReportType[] newArray(int size) {
            return new ReportType[size];
        }
    };

    @Override
    public String toString() {
        return nameEn;
    }
}
