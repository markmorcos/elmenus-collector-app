package com.elmenus.elmenuscollector.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.elmenus.elmenuscollector.R;
import com.elmenus.elmenuscollector.helpers.Constants;
import com.elmenus.elmenuscollector.network.EditProfileRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class MyProfileActivity extends BaseActivity {

    EditText editName;
    Button submitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        initNavigationDrawer(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        editName = (EditText) findViewById(R.id.edit_text_name);
        editName.setText(Constants.currentUser.getName());

        submitButton = (Button) findViewById(R.id.button_submit);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                newLoadingDialog(MyProfileActivity.this).show();
                EditProfileRequest request = new EditProfileRequest(Constants.currentUser.getId());
                request.addParameter(EditProfileRequest.PARAM_NAME, editName.getText().toString())
                        .addParameter(EditProfileRequest.PARAM_PASSWORD, Constants.currentUser.getPassword());
                request.setContext(MyProfileActivity.this);
                JsonObjectRequest volleyRequest = new JsonObjectRequest(Request.Method.POST,
                        request.getQueryString(), request.getJsonRequest(), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getJSONObject("data").optBoolean("done")) {
                                Constants.currentUser.setName(editName.getText().toString());
                                mAdapter.notifyDataSetChanged();
                                Toast.makeText(MyProfileActivity.this, "Profile updated", Toast.LENGTH_SHORT).show();
                            }
                            hideLoadingDialog();
                        } catch (JSONException e) {
                            hideLoadingDialog();
                            Toast.makeText(MyProfileActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideLoadingDialog();
                        Toast.makeText(MyProfileActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                        error.printStackTrace();
                    }
                });
                RequestQueue mRequestQueue = Volley.newRequestQueue(MyProfileActivity.this);
                mRequestQueue.add(volleyRequest);
            }
        });
    }
}
