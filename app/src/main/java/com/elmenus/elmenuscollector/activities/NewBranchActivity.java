package com.elmenus.elmenuscollector.activities;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.elmenus.elmenuscollector.R;
import com.elmenus.elmenuscollector.adapters.ModelSpinnerAdapter;
import com.elmenus.elmenuscollector.helpers.ImageItem;
import com.elmenus.elmenuscollector.models.Area;
import com.elmenus.elmenuscollector.models.Branch;
import com.elmenus.elmenuscollector.models.Feature;
import com.elmenus.elmenuscollector.models.Model;
import com.elmenus.elmenuscollector.network.GetAreasRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class NewBranchActivity extends BaseActivity {

    ArrayList<Model> areas;

    Button bChooseLocation;
    EditText eAddressEn;
    EditText eAddressAr;
    EditText eTelephone;
    Spinner sArea;
    Button bImages;
    Button bFeatures;
    Button saveButton;

    TextView tFeatures;

    LinearLayout llOpeningHours;

    public static final int REQUEST_CODE_LOCATION = 4;
    public static final int REQUEST_CODE_IMAGES = 5;
    public static final int REQUEST_CODE_FEATURES = 6;

    Branch branch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_branch);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        branch = getIntent().getParcelableExtra("result");
        if (branch == null) {
            branch = new Branch();
        }

        bChooseLocation = (Button) findViewById(R.id.choose_location);
        eAddressEn = (EditText) findViewById(R.id.address_en);
        eAddressAr = (EditText) findViewById(R.id.address_ar);
        eTelephone = (EditText) findViewById(R.id.telephone);
        sArea = (Spinner) findViewById(R.id.spinner_area);
        bImages = (Button) findViewById(R.id.button_images);
        bFeatures = (Button) findViewById(R.id.button_features);
        saveButton = (Button) findViewById(R.id.button_save);

        tFeatures = (TextView) findViewById(R.id.text_features);

        eAddressEn.setText(branch.getAddressEn());
        eAddressAr.setText(branch.getAddressAr());
        eTelephone.setText(branch.getPhoneNumber());
        tFeatures.setText(join(branch.getFeatures()));

        areas = new ArrayList<>();
        sArea.setAdapter(new ModelSpinnerAdapter(this, R.layout.layout_model_spinner_adapter, areas));
        GetAreasRequest request = new GetAreasRequest();
        newLoadingDialog(this).show();
        JsonObjectRequest volleyRequest = new JsonObjectRequest(Request.Method.GET,
                request.getQueryString(), request.getJsonRequest(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    areas.addAll(Area.parseAreasJsonArray(response.getJSONObject("data").getJSONArray("areas")));
                    ((ModelSpinnerAdapter) sArea.getAdapter()).notifyDataSetChanged();
                    if (areas.size() > 0) {
                        if (branch.getArea() != null) {
                            sArea.setSelection(0);
                            for (int i = 0; i < areas.size(); ++i) {
                                if (branch.getArea().equals(areas.get(i))) {
                                    sArea.setSelection(i);
                                    break;
                                }
                            }
                        } else {
                            branch.setArea((Area) areas.get(0));
                        }
                    }
                    hideLoadingDialog();
                } catch (JSONException e) {
                    hideLoadingDialog();
                    Toast.makeText(NewBranchActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(NewBranchActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                hideLoadingDialog();
            }
        });
        RequestQueue mRequestQueue = Volley.newRequestQueue(this);
        mRequestQueue.add(volleyRequest);

        bChooseLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(NewBranchActivity.this, LocationActivity.class), REQUEST_CODE_LOCATION);
            }
        });
        sArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                branch.setArea((Area) areas.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        bImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewBranchActivity.this, NewImagesActivity.class);
                intent.putExtra("result", branch.getImages());
                startActivityForResult(intent, REQUEST_CODE_IMAGES);
            }
        });
        bFeatures.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewBranchActivity.this, MultipleSelectionsActivity.class);
                intent.putExtra("result", branch.getFeatures());
                startActivityForResult(intent, REQUEST_CODE_FEATURES);
            }
        });
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                branch.setAddressEn(eAddressEn.getText().toString());
                branch.setAddressAr(eAddressAr.getText().toString());
                branch.setPhoneNumber(eTelephone.getText().toString());
                ArrayList<Branch.OpenDetail> details = new ArrayList<>();
                for (int i = 0; i < 7; ++i) {
                    String dayOfWeek = ((TextView) llOpeningHours.getChildAt(3 * i)).getText().toString();
                    String from = ((EditText) llOpeningHours.getChildAt(3 * i + 1)).getText().toString();
                    String to = ((EditText) llOpeningHours.getChildAt(3 * i + 2)).getText().toString();
                    Branch.OpenDetail detail = new Branch.OpenDetail(dayOfWeek);
                    detail.setFrom(from);
                    detail.setTo(to);
                    details.add(detail);
                }
                branch.setOpenDetails(details);
                String action = getIntent().getAction();
                Intent intent = new Intent();
                intent.setAction(action);
                if (action != null && action.equals("edit")) {
                    intent.putExtra("position", getIntent().getIntExtra("position", -1));
                }
                intent.putExtra("result", (Parcelable) branch);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        llOpeningHours = (LinearLayout) findViewById(R.id.linear_opening_hours);
        for (int i = 0; i < 7 - branch.getOpenDetails().size(); ++i) {
            branch.getOpenDetails().add(new Branch.OpenDetail(capitalise(Branch.daysOfWeek[i])));
        }
        for (Branch.OpenDetail detail : branch.getOpenDetails()) {
            TextView dayOfWeek = new TextView(this);
            dayOfWeek.setText(detail.getDayOfWeek());

            EditText from = new EditText(NewBranchActivity.this);
            from.setHint("From");
            from.setText(detail.getFrom());

            EditText to = new EditText(NewBranchActivity.this);
            to.setHint("To");
            to.setText(detail.getTo());

            llOpeningHours.addView(dayOfWeek);
            llOpeningHours.addView(from);
            llOpeningHours.addView(to);
        }
        llOpeningHours.requestLayout();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_LOCATION && resultCode == RESULT_OK) {
            String addressEn = data.getStringExtra("address_en");
            branch.setAddressEn(addressEn);
            eAddressEn.setText(addressEn);

            String addressAr = data.getStringExtra("address_ar");
            branch.setAddressAr(addressAr);
            eAddressAr.setText(addressAr);

            Location location = data.getParcelableExtra("location");
            branch.setLongitude(location.getLongitude());
            branch.setLatitude(location.getLatitude());
        }
        if (requestCode == REQUEST_CODE_IMAGES && resultCode == RESULT_OK) {
            ArrayList<ImageItem> result = data.getParcelableArrayListExtra("result");
            branch.setImages(result);
        }
        if (requestCode == REQUEST_CODE_FEATURES && resultCode == RESULT_OK) {
            ArrayList<Feature> result = data.getParcelableArrayListExtra("result");
            branch.setFeatures(result);
            tFeatures.setText(join(result));
        }
    }
}
