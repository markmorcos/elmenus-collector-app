package com.elmenus.elmenuscollector.activities;

import android.os.Bundle;

import com.elmenus.elmenuscollector.R;

public class HelpActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        initNavigationDrawer(this);
    }
}
