package com.elmenus.elmenuscollector.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.elmenus.elmenuscollector.R;
import com.elmenus.elmenuscollector.views.TouchableWrapper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

public class LocationActivity extends BaseActivity implements OnMapReadyCallback,
        TouchableWrapper.UpdateMapAfterUserInteraction {

    MapFragment mapFragment;
    GoogleMap googleMap;
    Location startLocation, currentLocation;
    boolean drawn;

    EditText editTextEn;
    EditText editTextAr;
    Button submitLocation;

    Location mLocation;
    CameraPosition cameraPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            new AlertDialog.Builder(this).setMessage("Please enable your GPS").setCancelable(false)
                    .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }).create().show();
            return;
        }
        newLoadingDialog(this).show();

        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.fragment_map);
        mapFragment.getMapAsync(this);

        editTextEn = (EditText) findViewById(R.id.edit_text_address_en);
        editTextAr = (EditText) findViewById(R.id.edit_text_address_ar);
        submitLocation = (Button) findViewById(R.id.button_save_location);
        submitLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("address_en", editTextEn.getText().toString());
                intent.putExtra("address_ar", editTextAr.getText().toString());
                intent.putExtra("location", mLocation);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        drawn = false;
    }

    void updateLocation() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        Location location = locationManager.getLastKnownLocation(provider);
        if(location != null) {
            drawMarker(location, false);
        }
    }

    private void drawMarker(final Location location, boolean zoom) {
        LatLng currentPosition = new LatLng(location.getLatitude(),location.getLongitude());
        if (zoom) {
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentPosition, 19));
        } else {
            googleMap.animateCamera(CameraUpdateFactory.newLatLng(currentPosition));
        }
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                drawMarker(startLocation, false);
                getAddress(startLocation);
                return true;
            }
        });
        googleMap.setMyLocationEnabled(true);
        googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(final CameraPosition cameraPosition) {
                LocationActivity.this.cameraPosition = cameraPosition;
            }
        });
        googleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {
                if (!drawn) {
                    hideLoadingDialog();
                    startLocation = currentLocation = location;
                    getAddress(location);
                    drawMarker(location, true);
                    drawn = true;
                }
            }
        });
        updateLocation();
    }

    void getAddress(final Location location) {
        newLoadingDialog(this).show();
        final RequestQueue mRequestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest volleyRequestEn = new JsonObjectRequest(Request.Method.GET,
                "https://maps.googleapis.com/maps/api/geocode/json?latlng="
                        + location.getLatitude() + "," + location.getLongitude()
                        + "&language=en&key=AIzaSyDvvRRgY0037X4UU0_OC3JzLqYlO2g9_tw", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            editTextEn.setText(response.getJSONArray("results").getJSONObject(0)
                                    .getString("formatted_address"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoadingDialog();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LocationActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                hideLoadingDialog();
            }
        });
        JsonObjectRequest volleyRequestAr = new JsonObjectRequest(Request.Method.GET,
                "https://maps.googleapis.com/maps/api/geocode/json?latlng="
                        + location.getLatitude() + "," + location.getLongitude()
                        + "&language=ar&key=AIzaSyDvvRRgY0037X4UU0_OC3JzLqYlO2g9_tw", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            editTextAr.setText(response.getJSONArray("results").getJSONObject(0)
                                    .getString("formatted_address"));
                            hideLoadingDialog();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            hideLoadingDialog();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LocationActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                hideLoadingDialog();
            }
        });
        mRequestQueue.add(volleyRequestEn);
        mRequestQueue.add(volleyRequestAr);
    }

    @Override
    public void onUpdateMapAfterUserInteraction() {
        if (startLocation != null && cameraPosition != null) {
            Location location = new Location("");
            location.setLatitude(cameraPosition.target.latitude);
            location.setLongitude(cameraPosition.target.longitude);
            if (currentLocation.getLatitude() != location.getLatitude()
                    || currentLocation.getLongitude() != location.getLongitude()) {
                float distance = startLocation.distanceTo(location);
                if (distance <= 1000) {
                    currentLocation = location;
                    getAddress(location);
                    drawMarker(location, false);
                } else {
                    drawMarker(currentLocation, false);
                    Toast.makeText(LocationActivity.this,
                            "Location too far from your current location",
                            Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
