package com.elmenus.elmenuscollector.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.elmenus.elmenuscollector.R;
import com.elmenus.elmenuscollector.models.Contact;

public class NewContactActivity extends BaseActivity {

    Contact contact;

    EditText name;
    EditText jobTitle;
    EditText mobileNumber;
    EditText email;
    Button submitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_contact);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        contact = getIntent().getParcelableExtra("result");
        if (contact == null) {
            contact = new Contact();
        }

        name = (EditText) findViewById(R.id.name);
        jobTitle = (EditText) findViewById(R.id.job_title);
        mobileNumber = (EditText) findViewById(R.id.mobile_number);
        email = (EditText) findViewById(R.id.email);
        submitButton = (Button) findViewById(R.id.button_submit);

        name.setText(contact.getName());
        jobTitle.setText(contact.getJobTitle());
        mobileNumber.setText(contact.getMobileNumber());
        email.setText(contact.getEmail());

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contact.setName(name.getText().toString());
                contact.setJobTitle(jobTitle.getText().toString());
                contact.setMobileNumber(mobileNumber.getText().toString());
                contact.setEmail(email.getText().toString());
                Intent intent = new Intent();
                String action = getIntent().getAction();
                intent.setAction(action);
                if (action.equals("edit")) {
                    intent.putExtra("position", getIntent().getIntExtra("position", -1));
                }
                intent.putExtra("result", (Parcelable) contact);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
