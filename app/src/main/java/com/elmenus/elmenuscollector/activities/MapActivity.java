package com.elmenus.elmenuscollector.activities;

import android.location.Location;
import android.os.Bundle;

import com.elmenus.elmenuscollector.R;
import com.elmenus.elmenuscollector.views.TouchableWrapper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapActivity extends BaseActivity implements OnMapReadyCallback,
        TouchableWrapper.UpdateMapAfterUserInteraction {

    MapFragment mapFragment;
    GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.fragment_map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.googleMap = googleMap;
        ArrayList<Location> locations = getIntent().getParcelableArrayListExtra("result");
        drawMarkers(locations);
    }

    private void drawMarkers(ArrayList<Location> locations) {
        googleMap.clear();
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Location location : locations) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            builder.include(latLng);
            googleMap.addMarker(new MarkerOptions()
                    .title(location.getProvider())
                    .position(latLng));
        }
        final LatLngBounds bounds = builder.build();
        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 20));
            }
        });
    }

    @Override
    public void onUpdateMapAfterUserInteraction() {
        // TODO nothing
    }
}
