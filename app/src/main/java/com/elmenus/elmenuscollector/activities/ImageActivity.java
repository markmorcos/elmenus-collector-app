package com.elmenus.elmenuscollector.activities;

import android.os.Bundle;
import android.widget.ImageView;

import com.elmenus.elmenuscollector.helpers.ImageItem;
import com.elmenus.elmenuscollector.R;

public class ImageActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        ImageItem item = getIntent().getExtras().getParcelable("image");
        ImageView mImageView = (ImageView) findViewById(R.id.image);
        mImageView.setImageBitmap(BaseActivity.rotateBitmap(item.getBitmap(), item.getRotation()));
    }
}
