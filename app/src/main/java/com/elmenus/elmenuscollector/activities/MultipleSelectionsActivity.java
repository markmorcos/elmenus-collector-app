package com.elmenus.elmenuscollector.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.elmenus.elmenuscollector.R;
import com.elmenus.elmenuscollector.adapters.MultipleSelectionsCheckBoxListViewAdapter;
import com.elmenus.elmenuscollector.models.Area;
import com.elmenus.elmenuscollector.models.Feature;
import com.elmenus.elmenuscollector.models.Cuisine;
import com.elmenus.elmenuscollector.models.Model;
import com.elmenus.elmenuscollector.network.GetAreasRequest;
import com.elmenus.elmenuscollector.network.GetCuisinesRequest;
import com.elmenus.elmenuscollector.network.GetFeaturesRequest;
import com.elmenus.elmenuscollector.network.WebServiceRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MultipleSelectionsActivity extends BaseActivity {

    EditText filterText;
    ListView mItems;
    Button submitButton;
    ArrayList<Model> items;
    int requestCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiple_selections);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        filterText = (EditText) findViewById(R.id.filter_text);
        mItems = (ListView) findViewById(R.id.areas);

        mItems.setTextFilterEnabled(true);

        filterText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count < before) {
                    ((MultipleSelectionsCheckBoxListViewAdapter) mItems.getAdapter()).resetData();
                }
                ((MultipleSelectionsCheckBoxListViewAdapter) mItems.getAdapter()).getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        items = new ArrayList<>();
        mItems.setAdapter(new MultipleSelectionsCheckBoxListViewAdapter(this, R.layout.layout_multiple_selections_checkbox_listview_adapter, items));

        requestCode = getIntent().getIntExtra("requestCode", 0);
        WebServiceRequest request;
        JsonObjectRequest volleyRequest;
        RequestQueue mRequestQueue = Volley.newRequestQueue(this);
        newLoadingDialog(this).show();
        switch (requestCode) {
            case NewRestaurantActivity.REQUEST_CODE_DELIVERS_TO:
                request = new GetAreasRequest();
                volleyRequest = new JsonObjectRequest(Request.Method.GET,
                        request.getQueryString(), request.getJsonRequest(), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            items.addAll(Area.parseAreasJsonArray(response.getJSONObject("data").getJSONArray("areas")));
                            mItems.setAdapter(new MultipleSelectionsCheckBoxListViewAdapter(MultipleSelectionsActivity.this, R.layout.layout_multiple_selections_checkbox_listview_adapter, items));
                            hideLoadingDialog();
                        } catch (JSONException e) {
                            Toast.makeText(MultipleSelectionsActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            hideLoadingDialog();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MultipleSelectionsActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                        hideLoadingDialog();
                    }
                });
                mRequestQueue.add(volleyRequest);
                break;
            case NewRestaurantActivity.REQUEST_CODE_CUISINES:
                request = new GetCuisinesRequest();
                volleyRequest = new JsonObjectRequest(Request.Method.GET,
                        request.getQueryString(), request.getJsonRequest(), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            items.addAll(Cuisine.parseCuisineJsonArray(response.getJSONArray("data")));
                            mItems.setAdapter(new MultipleSelectionsCheckBoxListViewAdapter(MultipleSelectionsActivity.this, R.layout.layout_multiple_selections_checkbox_listview_adapter, items));
                            hideLoadingDialog();
                        } catch (JSONException e) {
                            Toast.makeText(MultipleSelectionsActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            hideLoadingDialog();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MultipleSelectionsActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                        hideLoadingDialog();
                    }
                });
                mRequestQueue.add(volleyRequest);
                break;
            case NewBranchActivity.REQUEST_CODE_FEATURES:
                request = new GetFeaturesRequest();
                volleyRequest = new JsonObjectRequest(Request.Method.GET,
                        request.getQueryString(), request.getJsonRequest(), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            items.addAll(Feature.parseFeaturesJsonArray(response.getJSONArray("data")));
                            mItems.setAdapter(new MultipleSelectionsCheckBoxListViewAdapter(MultipleSelectionsActivity.this, R.layout.layout_multiple_selections_checkbox_listview_adapter, items));
                            hideLoadingDialog();
                        } catch (JSONException e) {
                            Toast.makeText(MultipleSelectionsActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            hideLoadingDialog();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MultipleSelectionsActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                        hideLoadingDialog();
                    }
                });
                mRequestQueue.add(volleyRequest);
                break;
        }

        submitButton = (Button) findViewById(R.id.submit_button);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                if (requestCode == NewRestaurantActivity.REQUEST_CODE_DELIVERS_TO) {
                    ArrayList<Area> result = new ArrayList<>();
                    ArrayList<Model> temp = ((MultipleSelectionsCheckBoxListViewAdapter) mItems.getAdapter()).getSelectedItems();
                    for (Model model : temp) {
                        result.add((Area) model);
                    }
                    intent.putExtra("result", result);
                    setResult(RESULT_OK, intent);
                    finish();
                } else if (requestCode == NewRestaurantActivity.REQUEST_CODE_CUISINES) {
                    ArrayList<Cuisine> result = new ArrayList<>();
                    ArrayList<Model> temp = ((MultipleSelectionsCheckBoxListViewAdapter) mItems.getAdapter()).getSelectedItems();
                    for (Model model : temp) {
                        result.add((Cuisine) model);
                    }
                    intent.putExtra("result", result);
                    setResult(RESULT_OK, intent);
                    finish();
                } else if (requestCode == NewBranchActivity.REQUEST_CODE_FEATURES) {
                    ArrayList<Feature> result = new ArrayList<>();
                    ArrayList<Model> temp = ((MultipleSelectionsCheckBoxListViewAdapter) mItems.getAdapter()).getSelectedItems();
                    for (Model model : temp) {
                        result.add((Feature) model);
                    }
                    intent.putExtra("result", result);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });
    }
}
