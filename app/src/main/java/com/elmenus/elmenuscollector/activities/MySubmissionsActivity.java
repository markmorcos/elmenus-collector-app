package com.elmenus.elmenuscollector.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.elmenus.elmenuscollector.R;

public class MySubmissionsActivity extends BaseActivity {

    Button availableRestaurantsButton;
    Button myCollectionsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_submissions);
        initNavigationDrawer(this);

        availableRestaurantsButton = (Button) findViewById(R.id.button_available_restaurants);
        availableRestaurantsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MySubmissionsActivity.this, AvailableRestaurantsActivity.class));
                finish();
            }
        });
        myCollectionsButton = (Button) findViewById(R.id.button_my_collections);
        myCollectionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MySubmissionsActivity.this, MyCollectionsActivity.class));
                finish();
            }
        });
    }
}
