package com.elmenus.elmenuscollector.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andexert.expandablelayout.library.ExpandableLayout;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.elmenus.elmenuscollector.R;
import com.elmenus.elmenuscollector.helpers.Constants;
import com.elmenus.elmenuscollector.helpers.ImageItem;
import com.elmenus.elmenuscollector.models.Area;
import com.elmenus.elmenuscollector.models.Branch;
import com.elmenus.elmenuscollector.models.Contact;
import com.elmenus.elmenuscollector.models.Cuisine;
import com.elmenus.elmenuscollector.models.Feature;
import com.elmenus.elmenuscollector.models.Restaurant;
import com.elmenus.elmenuscollector.network.GetRestaurantsAutocompleteRequest;
import com.elmenus.elmenuscollector.network.SubmitBranchImageRequest;
import com.elmenus.elmenuscollector.network.SubmitBranchRequest;
import com.elmenus.elmenuscollector.network.SubmitContactRequest;
import com.elmenus.elmenuscollector.network.SubmitMenuImageRequest;
import com.elmenus.elmenuscollector.network.SubmitMenuRequest;
import com.elmenus.elmenuscollector.network.SubmitRestaurantRequest;
import com.fedorvlasov.lazylist.ImageLoader;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

public class NewRestaurantActivity extends BaseActivity {

    ImageView logo;
    File logoFile;
    Bitmap logoBitmap;
    Button changeLogo;

    AutoCompleteTextView eRestaurantNameEn;
    AutoCompleteTextView eRestaurantNameAr;
    EditText eTelephone;

    Button bImages;

    Button bNewContact;
    Button bNewBranch;
    Button bDeliversTo;
    Button bCuisines;

    LinearLayout llContacts;
    LinearLayout llBranches;
    TextView tDeliversTo;
    TextView tCuisines;

    Button saveButton;
    Button submitButton;

    public static final int REQUEST_CODE_BRANCH = 1;
    public static final int REQUEST_CODE_IMAGES = 11;
    public static final int REQUEST_CODE_CONTACT = 12;
    public static final int REQUEST_CODE_DELIVERS_TO = 2;
    public static final int REQUEST_CODE_CUISINES = 3;
    public static final int REQUEST_CODE_LOGO = 10;

    public static final String URL_LOGO = "http://s3.amazonaws.com/elmenusV3/Photos/Normal/";

    Restaurant restaurant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_restaurant);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        llContacts = (LinearLayout) findViewById(R.id.linear_contacts);
        llBranches = (LinearLayout) findViewById(R.id.linear_branches);
        tDeliversTo = (TextView) findViewById(R.id.text_delivers_to);
        tCuisines = (TextView) findViewById(R.id.text_cuisines);

        String action = getIntent().getAction();
        if (action != null && action.equals("edit")) {
            int position = getIntent().getIntExtra("position", -1);
            if (position != -1) {
                restaurant = Constants.collections.get(position);
                for (int i = 0; i < restaurant.getContacts().size(); ++i) {
                    addContact(restaurant.getContacts().get(i), i);
                }
                for (int i = 0; i < restaurant.getBranches().size(); ++i) {
                    addBranch(restaurant.getBranches().get(i), i);
                }
                tDeliversTo.setText(join(restaurant.getDeliveryAreas()));
                tCuisines.setText(join(restaurant.getCuisines()));
            } else {
                restaurant = new Restaurant();
            }
        } else {
            restaurant = new Restaurant();
        }

        logo = (ImageView) findViewById(R.id.logo);
        changeLogo = (Button) findViewById(R.id.button_change_logo);

        eRestaurantNameEn = (AutoCompleteTextView) findViewById(R.id.restaurant_name_en);
        eRestaurantNameAr = (AutoCompleteTextView) findViewById(R.id.restaurant_name_ar);
        eTelephone = (EditText) findViewById(R.id.telephone);

        if (restaurant.getNameEn() == null) {
            restaurant.setNameEn("");
        }
        if (restaurant.getNameAr() == null) {
            restaurant.setNameAr("");
        }
        eRestaurantNameEn.setText(restaurant.getNameEn());
        eRestaurantNameAr.setText(restaurant.getNameAr());
        eTelephone.setText(restaurant.getShortNumber());

        eRestaurantNameEn.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                GetRestaurantsAutocompleteRequest request = new GetRestaurantsAutocompleteRequest();
                request.addParameter(GetRestaurantsAutocompleteRequest.PARAM_CITY_ID, String.valueOf(Constants.currentUser.getCityId()));
                request.addParameter(GetRestaurantsAutocompleteRequest.PARAM_QUERY, StringEscapeUtils.escapeHtml4(s.toString()));
                JsonObjectRequest volleyRequest = new JsonObjectRequest(Request.Method.GET,
                        request.getQueryString(), request.getJsonRequest(), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray array = response.getJSONArray("data");
                            ArrayList<String> items = new ArrayList<>();
                            final ArrayList<String> itemsAr = new ArrayList<>();
                            for (int i = 0; i < array.length(); ++i) {
                                items.add(array.getJSONObject(i).getString("name.en"));
                                itemsAr.add(array.getJSONObject(i).getString("name_ar"));
                            }
                            eRestaurantNameEn.setAdapter(new ArrayAdapter<>(NewRestaurantActivity.this, android.R.layout.simple_dropdown_item_1line, items));
                            eRestaurantNameEn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    System.out.println(itemsAr.get(position));
                                    eRestaurantNameAr.setText(itemsAr.get(position));
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });
                RequestQueue mRequestQueue = Volley.newRequestQueue(NewRestaurantActivity.this);
                mRequestQueue.add(volleyRequest);
            }
        });
        eRestaurantNameAr.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                GetRestaurantsAutocompleteRequest request = new GetRestaurantsAutocompleteRequest();
                request.addParameter(GetRestaurantsAutocompleteRequest.PARAM_CITY_ID, String.valueOf(Constants.currentUser.getCityId()));
                request.addParameter(GetRestaurantsAutocompleteRequest.PARAM_QUERY, StringEscapeUtils.escapeHtml4(s.toString()));
                JsonObjectRequest volleyRequest = new JsonObjectRequest(Request.Method.GET,
                        request.getQueryString(), request.getJsonRequest(), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray array = response.getJSONArray("data");
                            final ArrayList<String> items = new ArrayList<>();
                            ArrayList<String> itemsAr = new ArrayList<>();
                            for (int i = 0; i < array.length(); ++i) {
                                items.add(array.getJSONObject(i).getString("name.en"));
                                itemsAr.add(array.getJSONObject(i).getString("name_ar"));
                            }
                            eRestaurantNameAr.setAdapter(new ArrayAdapter<>(NewRestaurantActivity.this, android.R.layout.simple_dropdown_item_1line, itemsAr));
                            eRestaurantNameAr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    System.out.println(items.get(position));
                                    eRestaurantNameEn.setText(items.get(position));
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });
                RequestQueue mRequestQueue = Volley.newRequestQueue(NewRestaurantActivity.this);
                mRequestQueue.add(volleyRequest);
            }
        });

        bImages = (Button) findViewById(R.id.button_images);
        bNewContact = (Button) findViewById(R.id.new_contact);
        bNewBranch = (Button) findViewById(R.id.new_branch);
        bDeliversTo = (Button) findViewById(R.id.delivers_to);
        bCuisines = (Button) findViewById(R.id.cuisines);

        saveButton = (Button) findViewById(R.id.button_save);
        submitButton = (Button) findViewById(R.id.button_submit);

        if (!restaurant.getLogo().equals("") && !restaurant.isLogoEdited()) {
            ImageLoader  imageLoader = new ImageLoader(this);
            imageLoader.displayImage(URL_LOGO + restaurant.getLogo(), logo);
        } else if (restaurant.getLogoFile() != null && restaurant.getLogoFile().exists()) {
            logo.setImageBitmap(cropToSquare(BitmapFactory.decodeFile(restaurant.getLogoFile().getAbsolutePath())));
        }

        changeLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchChoosePictureIntent();
            }
        });

        bImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewRestaurantActivity.this, NewImagesActivity.class);
                intent.putParcelableArrayListExtra("result", restaurant.getImages());
                startActivityForResult(intent, REQUEST_CODE_IMAGES);
            }
        });

        bNewContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewRestaurantActivity.this, NewContactActivity.class);
                intent.setAction("add");
                startActivityForResult(intent, REQUEST_CODE_CONTACT);
            }
        });

        bNewBranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewRestaurantActivity.this, NewBranchActivity.class);
                intent.setAction("add");
                startActivityForResult(intent, REQUEST_CODE_BRANCH);
            }
        });

        bDeliversTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewRestaurantActivity.this, MultipleSelectionsActivity.class);
                intent.putParcelableArrayListExtra("result", restaurant.getDeliveryAreas());
                startActivityForResult(intent, REQUEST_CODE_DELIVERS_TO);
            }
        });

        bCuisines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewRestaurantActivity.this, MultipleSelectionsActivity.class);
                intent.putParcelableArrayListExtra("result", restaurant.getCuisines());
                startActivityForResult(intent, REQUEST_CODE_CUISINES);
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restaurant.setNameEn(eRestaurantNameEn.getText().toString());
                restaurant.setNameAr(eRestaurantNameAr.getText().toString());
                restaurant.setShortNumber(eTelephone.getText().toString());
                Intent intent = new Intent();
                String action = getIntent().getAction();
                intent.setAction(action);
                if (action != null && action.equals("edit")) {
                    intent.putExtra("position", getIntent().getIntExtra("position", -1));
                }
                intent.putExtra("result", (Parcelable) restaurant);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restaurant.setNameEn(eRestaurantNameEn.getText().toString());
                restaurant.setNameAr(eRestaurantNameAr.getText().toString());
                restaurant.setShortNumber(eTelephone.getText().toString());
                submitRestaurant();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_LOGO && resultCode == RESULT_OK) {
            Uri tempUri = data.getData();
            File file = new File(getRealPathFromURI(this, tempUri));
            logoFile = file;
            restaurant.setLogoFile(file);
            restaurant.setLogoEdited(true);
            logoBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            logo.setImageBitmap(cropToSquare(logoBitmap));
        }
        if (requestCode == REQUEST_CODE_IMAGES && resultCode == RESULT_OK) {
            ArrayList<ImageItem> result = data.getParcelableArrayListExtra("result");
            restaurant.setImages(result);
        }
        if (requestCode == REQUEST_CODE_CONTACT && resultCode == RESULT_OK) {
            final Contact contact = data.getParcelableExtra("result");
            String action = data.getAction();
            if (action.equals("add")) {
                contact.setRestaurantId(restaurant.getId());
                restaurant.getContacts().add(contact);
                addContact(contact, restaurant.getContacts().size() - 1);
            } else if (action.equals("edit")) {
                int position = data.getIntExtra("position", -1);
                if (position != -1) {
                    restaurant.getContacts().remove(position);
                    llContacts.removeViewAt(position);
                    restaurant.getContacts().add(position, contact);
                    addContact(contact, position);
                }
            }
        }
        if (requestCode == REQUEST_CODE_BRANCH && resultCode == RESULT_OK) {
            final Branch branch = data.getParcelableExtra("result");
            String action = data.getAction();
            if (action.equals("add")) {
                branch.setRestaurantId(restaurant.getId());
                restaurant.getBranches().add(branch);
                addBranch(branch, restaurant.getBranches().size() - 1);
            } else if (action.equals("edit")) {
                int position = data.getIntExtra("position", -1);
                if (position != -1) {
                    restaurant.getBranches().remove(position);
                    llBranches.removeViewAt(position);
                    restaurant.getBranches().add(position, branch);
                    addBranch(branch, position);
                }
            }
        }
        if (requestCode == REQUEST_CODE_DELIVERS_TO && resultCode == RESULT_OK) {
            ArrayList<Area> result = data.getParcelableArrayListExtra("result");
            restaurant.setDeliveryAreas(result);
            tDeliversTo.setText(join(result));
        }
        if (requestCode == REQUEST_CODE_CUISINES && resultCode == RESULT_OK) {
            ArrayList<Cuisine> result = data.getParcelableArrayListExtra("result");
            restaurant.setCuisines(result);
            tCuisines.setText(join(result));
        }
    }

    private void dispatchChoosePictureIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CODE_LOGO);
    }

    private void addContact(final Contact contact, final int position) {
        View view = LayoutInflater.from(this).inflate(R.layout.layout_contacts, null);
        TextView text = (TextView) view.findViewById(R.id.text);
        text.setText(contact.toString());
        Button edit = (Button) view.findViewById(R.id.button_edit);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewRestaurantActivity.this, NewContactActivity.class);
                intent.setAction("edit");
                intent.putExtra("position", position);
                intent.putExtra("result", (Parcelable) contact);
                startActivityForResult(intent, REQUEST_CODE_CONTACT);
            }
        });
        Button delete = (Button) view.findViewById(R.id.button_delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(NewRestaurantActivity.this)
                        .setMessage("Are you sure you want to delete this item?")
                        .setNegativeButton("No", null)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                restaurant.getContacts().remove(position);
                                llContacts.removeViewAt(position);
                            }
                        }).create().show();
            }
        });
        llContacts.addView(view);
    }

    private void addBranch(final Branch branch, final int position) {
        final ExpandableLayout view = (ExpandableLayout) LayoutInflater.from(this).inflate(R.layout.layout_branches, null);
        TextView text = (TextView) view.getHeaderRelativeLayout().findViewById(R.id.text);
        text.setText(branch.toString());
        Button edit = (Button) view.getHeaderRelativeLayout().findViewById(R.id.button_edit);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewRestaurantActivity.this, NewBranchActivity.class);
                intent.setAction("edit");
                intent.putExtra("position", position);
                intent.putExtra("result", (Parcelable) branch);
                startActivityForResult(intent, REQUEST_CODE_BRANCH);
            }
        });
        Button delete = (Button) view.getHeaderRelativeLayout().findViewById(R.id.button_delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(NewRestaurantActivity.this)
                        .setMessage("Are you sure you want to delete this item?")
                        .setNegativeButton("No", null)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                restaurant.getBranches().remove(position);
                                llBranches.removeViewAt(position);
                            }
                        }).create().show();
            }
        });
        final Button toggle = (Button) view.getHeaderRelativeLayout().findViewById(R.id.button_toggle);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view.isOpened()) {
                    toggle.setText("▽");
                } else {
                    toggle.setText("△");
                }
            }
        });
        toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (view.isOpened()) {
                    view.hide();
                    toggle.setText("▽");
                } else {
                    view.show();
                    toggle.setText("△");
                }
            }
        });
        TextView address = (TextView) view.getContentRelativeLayout().findViewById(R.id.address);
        address.setText(branch.getAddressEn());
        llBranches.addView(view, position);
    }

    private void submitRestaurant() {
        SubmitRestaurantRequest request = new SubmitRestaurantRequest();
        ArrayList<Long> cuisineIds = new ArrayList<>();
        for (Cuisine cuisine : restaurant.getCuisines()) {
            cuisineIds.add(cuisine.getId());
        }
        ArrayList<Long> deliveryAreaIds = new ArrayList<>();
        for (Area area : restaurant.getDeliveryAreas()) {
            deliveryAreaIds.add(area.getId());
        }
        request.addParameter(SubmitRestaurantRequest.PARAM_CITY_ID, String.valueOf(Constants.currentUser.getCityId()));
        request.addParameter(SubmitRestaurantRequest.PARAM_CUISINES, join(cuisineIds));
        request.addParameter(SubmitRestaurantRequest.PARAM_DELIVERY_AREAS, join(deliveryAreaIds));
        request.addParameter(SubmitRestaurantRequest.PARAM_NAME_EN, restaurant.getNameEn());
        request.addParameter(SubmitRestaurantRequest.PARAM_NAME_AR, restaurant.getNameAr());
        request.addParameter(SubmitRestaurantRequest.PARAM_SHORT_NUMBER, restaurant.getShortNumber());
        if (restaurant.isLogoEdited()) {
            request.addParameter(SubmitRestaurantRequest.PARAM_LOGO, logoFile.getAbsolutePath());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            logoBitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
            byte[] b = baos.toByteArray();
            request.addParameter(SubmitRestaurantRequest.PARAM_LOGO, Base64.encodeToString(b, 0));
        }
        request.addParameter(SubmitRestaurantRequest.PARAM_PICKUP_SOURCE, "collector");
        request.addParameter(SubmitRestaurantRequest.PARAM_EDIT_RESTAURANT,
                getIntent().getAction().equals("edit") ? String.valueOf(restaurant.getId()) : "0");
        newLoadingDialog(NewRestaurantActivity.this).show();
        JsonObjectRequest volleyRequest = new JsonObjectRequest(Request.Method.POST,
                request.getQueryString(), request.getJsonRequest(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println(response);
                try {
                    submitMenu(response.getJSONObject("data").getLong("id"));
                } catch (Exception e) {
                    Toast.makeText(NewRestaurantActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    hideLoadingDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(NewRestaurantActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                hideLoadingDialog();
            }
        });
        /*
        Iterator<String> keys = request.getJsonRequest().keys();
        while (keys.hasNext()) {
            try {
                String key = keys.next();
                String value = request.getJsonRequest().getString(key);
                volleyRequest.addMultipartParam(key, value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        volleyRequest.addMultipartParam(SubmitRestaurantRequest.PARAM_CITY_ID, String.valueOf(Constants.currentUser.getCityId()));
        volleyRequest.addMultipartParam(SubmitRestaurantRequest.PARAM_CUISINES, join(cuisineIds));
        volleyRequest.addMultipartParam(SubmitRestaurantRequest.PARAM_DELIVERY_AREAS, join(deliveryAreaIds));
        volleyRequest.addMultipartParam(SubmitRestaurantRequest.PARAM_NAME_EN, restaurant.getNameEn());
        volleyRequest.addMultipartParam(SubmitRestaurantRequest.PARAM_NAME_AR, restaurant.getNameAr());
        volleyRequest.addMultipartParam(SubmitRestaurantRequest.PARAM_SHORT_NUMBER, restaurant.getShortNumber());
        if (restaurant.isLogoEdited()) {
            volleyRequest.addFile(SubmitRestaurantRequest.PARAM_LOGO, logoFile.getAbsolutePath());
        }
        volleyRequest.addMultipartParam(SubmitRestaurantRequest.PARAM_PICKUP_SOURCE, "collector");
        volleyRequest.addMultipartParam(SubmitRestaurantRequest.PARAM_EDIT_RESTAURANT,
                getIntent().getAction().equals("edit") ? String.valueOf(restaurant.getId()) : "0");
        */
        RequestQueue mRequestQueue = Volley.newRequestQueue(NewRestaurantActivity.this);
        mRequestQueue.add(volleyRequest);
    }

    private void submitMenu(final long restaurantId) {
        SubmitMenuRequest request = new SubmitMenuRequest();
        request.addParameter(SubmitMenuRequest.PARAM_RESTAURANT_ID, String.valueOf(restaurantId));
        request.addParameter(SubmitMenuRequest.PARAM_TITLE_EN, "Main");
        request.addParameter(SubmitMenuRequest.PARAM_TITLE_AR, "المنيو");
        JsonObjectRequest volleyRequest = new JsonObjectRequest(Request.Method.POST,
                request.getQueryString(), request.getJsonRequest(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println(response);
                try {
                    submitMenuImages(restaurantId, response.getJSONObject("data").getLong("id"));
                } catch (JSONException e) {
                    Toast.makeText(NewRestaurantActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    hideLoadingDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(NewRestaurantActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                hideLoadingDialog();
            }
        });
        RequestQueue mRequestQueue = Volley.newRequestQueue(NewRestaurantActivity.this);
        mRequestQueue.add(volleyRequest);
    }

    int c;

    private void submitMenuImages(final long restaurantId, long menuId) {
        c = 0;
        RequestQueue mRequestQueue = Volley.newRequestQueue(NewRestaurantActivity.this);
        for (ImageItem image : restaurant.getImages()) {
            SubmitMenuImageRequest request = new SubmitMenuImageRequest(menuId);
            request.addParameter(SubmitMenuImageRequest.PARAM_USER_ID, String.valueOf(Constants.currentUser.getId()));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            image.getBitmap().compress(Bitmap.CompressFormat.JPEG, 80, baos);
            byte[] b = baos.toByteArray();
            request.addParameter(SubmitMenuImageRequest.PARAM_FILE, Base64.encodeToString(b, 0));
            request.addParameter(SubmitMenuImageRequest.PARAM_REAL_NAME, image.getFile().getName());
            JsonObjectRequest volleyRequest = new JsonObjectRequest(Request.Method.POST,
                    request.getQueryString(), request.getJsonRequest(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    System.out.println(response);
                    if (++c == restaurant.getImages().size()) {
                        submitContacts(restaurantId);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(NewRestaurantActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                    hideLoadingDialog();
                }
            });
            mRequestQueue.add(volleyRequest);
        }
    }

    int d;

    private void submitContacts(final long restaurantId) {
        d = 0;
        RequestQueue mRequestQueue = Volley.newRequestQueue(NewRestaurantActivity.this);
        for (Contact contact : restaurant.getContacts()) {
            SubmitContactRequest request = new SubmitContactRequest();
            request.addParameter(SubmitContactRequest.PARAM_RESTAURANT_ID, String.valueOf(restaurantId));
            request.addParameter(SubmitContactRequest.PARAM_NAME, contact.getName());
            request.addParameter(SubmitContactRequest.PARAM_EMAIL, contact.getEmail());
            request.addParameter(SubmitContactRequest.PARAM_MOBILE_NUMBER, contact.getMobileNumber());
            request.addParameter(SubmitContactRequest.PARAM_JOB_TITLE, contact.getJobTitle());
            request.addParameter(SubmitContactRequest.PARAM_NOTES, "");
            JsonObjectRequest volleyRequest = new JsonObjectRequest(Request.Method.POST,
                    request.getQueryString(), request.getJsonRequest(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    System.out.println(response);
                    if (++d == restaurant.getContacts().size()) {
                        submitBranches(restaurantId);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(NewRestaurantActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                    hideLoadingDialog();
                }
            });
            mRequestQueue.add(volleyRequest);
        }
    }

    int total;

    private void submitBranches(long restaurantId) {
        total = 0;
        for (Branch branch : restaurant.getBranches()) {
            total += branch.getImages().size();
        }
        RequestQueue mRequestQueue = Volley.newRequestQueue(NewRestaurantActivity.this);
        for (int i = 0; i < restaurant.getBranches().size(); ++i) {
            final int j = i;
            Branch branch = restaurant.getBranches().get(i);
            SubmitBranchRequest request = new SubmitBranchRequest();
            request.addParameter(SubmitBranchRequest.PARAM_RESTAURANT_ID, String.valueOf(restaurantId));
            request.addParameter(SubmitBranchRequest.PARAM_PHONE_NUMBER, branch.getPhoneNumber());
            request.addParameter(SubmitBranchRequest.PARAM_ADDRESS_EN, branch.getAddressEn());
            request.addParameter(SubmitBranchRequest.PARAM_ADDRESS_AR, branch.getAddressAr());
            ArrayList<Long> featureIds = new ArrayList<>();
            for (Feature feature : branch.getFeatures()) {
                featureIds.add(feature.getId());
            }
            request.addParameter(SubmitBranchRequest.PARAM_FEATURES, join(featureIds));
            request.addParameter(SubmitBranchRequest.PARAM_AREA, String.valueOf(branch.getArea().getId()));
            request.addParameter(SubmitBranchRequest.PARAM_LONGITUDE, String.valueOf(branch.getLongitude()));
            request.addParameter(SubmitBranchRequest.PARAM_LATITUDE, String.valueOf(branch.getLatitude()));
            String openingHours = "{";
            for (Branch.OpenDetail openDetail : branch.getOpenDetails()) {
                openingHours += "{\"" + openDetail.getDayOfWeek() +"\":{\"from\":\"" + openDetail.getFrom() + "\",\"to\":\"" + openDetail.getTo() + "\"}}";
            }
            openingHours += "}";
            request.addParameter(SubmitBranchRequest.PARAM_OPENING_HOURS, openingHours);
            JsonObjectRequest volleyRequest = new JsonObjectRequest(Request.Method.POST,
                    request.getQueryString(), request.getJsonRequest(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    System.out.println(response);
                    try {
                        submitBranchImages(j, response.getJSONObject("data").getLong("id"));
                    } catch (JSONException e) {
                        Toast.makeText(NewRestaurantActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(NewRestaurantActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                    hideLoadingDialog();
                }
            });
            mRequestQueue.add(volleyRequest);
        }
    }

    int f;

    private void submitBranchImages(int index, long branchId) {
        f = 0;
        RequestQueue mRequestQueue = Volley.newRequestQueue(NewRestaurantActivity.this);
        for (ImageItem image : restaurant.getBranches().get(index).getImages()) {
            SubmitBranchImageRequest request = new SubmitBranchImageRequest(branchId);
            request.addParameter(SubmitBranchImageRequest.PARAM_USER_ID, String.valueOf(Constants.currentUser.getId()));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            image.getBitmap().compress(Bitmap.CompressFormat.JPEG, 80, baos);
            byte[] b = baos.toByteArray();
            request.addParameter(SubmitBranchImageRequest.PARAM_FILE, Base64.encodeToString(b, 0));
            request.addParameter(SubmitBranchImageRequest.PARAM_REAL_NAME, image.getFile().getName());
            JsonObjectRequest volleyRequest = new JsonObjectRequest(Request.Method.POST,
                    request.getQueryString(), request.getJsonRequest(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    System.out.println(response);
                    if (++f == total) {
                        restaurant.setStatus("submitted");
                        Intent intent = new Intent();
                        String action = getIntent().getAction();
                        intent.setAction(action);
                        if (action != null && action.equals("edit")) {
                            intent.putExtra("position", getIntent().getIntExtra("position", -1));
                        }
                        intent.putExtra("result", (Parcelable) restaurant);
                        setResult(RESULT_OK, intent);
                        finish();
                        hideLoadingDialog();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(NewRestaurantActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                    hideLoadingDialog();
                }
            });
            mRequestQueue.add(volleyRequest);
        }
    }
}
