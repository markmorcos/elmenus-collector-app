package com.elmenus.elmenuscollector.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.elmenus.elmenuscollector.R;
import com.elmenus.elmenuscollector.adapters.NavigationDrawerAdapter;
import com.elmenus.elmenuscollector.helpers.Constants;
import com.elmenus.elmenuscollector.helpers.Preferences;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class BaseActivity extends AppCompatActivity {

    ProgressDialog mProgressDialog;

    String[] titles;
    int[] icons = {R.drawable.tuba_48, R.drawable.tuba_48, R.drawable.tuba_48, R.drawable.tuba_48,
            R.drawable.tuba_48, R.drawable.tuba_48};
    Class CLASSES[] = {MyProfileActivity.class, MainActivity.class,
            AvailableRestaurantsActivity.class, MyCollectionsActivity.class,
            MySubmissionsActivity.class, MyPaymentsActivity.class, HelpActivity.class};

    RecyclerView mRecyclerView;

    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    DrawerLayout mDrawerLayout;
    ActionBarDrawerToggle mDrawerToggle;

    Toolbar toolbar;

    GestureDetector mGestureDetector;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_logout) {
            Constants.currentUser = null;
            Constants.accessToken = "";
            SharedPreferences sharedPreferences = Preferences.getPrefs(this);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("email", "");
            editor.putString("password", "");
            editor.apply();
            finish();
            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] project = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  project, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public ProgressDialog newLoadingDialog(Context context) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.getWindow().setGravity(Gravity.CENTER);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCancelable(false);
        return mProgressDialog;
    }

    public void hideLoadingDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.hide();
        }
    }

    public String join(ArrayList arrayList) {
        if (arrayList == null || arrayList.size() == 0) {
            return "";
        }
        String output = arrayList.get(0).toString();
        for(int i = 1, l = arrayList.size(); i < l; ++i) {
            output += ", " + arrayList.get(i);
        }
        return output;
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }


    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        intent.putExtra("requestCode", requestCode);
        super.startActivityForResult(intent, requestCode);
    }

    public void saveCollections() {
        try {
            FileOutputStream fileOut = openFileOutput("collections.ser", MODE_PRIVATE);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(Constants.collections);
            out.close();
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Bitmap cropToSquare(Bitmap bitmap) {
        int width  = bitmap.getWidth();
        int height = bitmap.getHeight();
        int newWidth = (height > width) ? width : height;
        int newHeight = (height > width) ? height - (height - width) : height;
        int cropW = (width - height) / 2;
        cropW = (cropW < 0) ? 0 : cropW;
        int cropH = (height - width) / 2;
        cropH = (cropH < 0) ? 0 : cropH;
        return Bitmap.createBitmap(bitmap, cropW, cropH, newWidth, newHeight);
    }

    public void initToolbar(AppCompatActivity context) {
        toolbar = (Toolbar) context.findViewById(R.id.tool_bar);
        context.setSupportActionBar(toolbar);
        try {
            ActivityInfo activityInfo = context.getPackageManager().getActivityInfo(getComponentName(), PackageManager.GET_META_DATA);
            String title = activityInfo.loadLabel(getPackageManager()).toString();
            toolbar.setTitle(title);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void initNavigationDrawer(final AppCompatActivity context) {
        initToolbar(context);

        titles = new String[]{context.getString(R.string.title_activity_main),
                context.getString(R.string.title_activity_available_restaurants),
                context.getString(R.string.title_activity_my_collections),
                context.getString(R.string.title_activity_my_submissions),
                context.getString(R.string.title_activity_my_payments),
                context.getString(R.string.title_activity_help)};

        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });

        mRecyclerView = (RecyclerView) context.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new NavigationDrawerAdapter(context, titles, icons);

        mRecyclerView.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mDrawerLayout = (DrawerLayout) context.findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(context, mDrawerLayout, toolbar, R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        mRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                View child = rv.findChildViewUnder(e.getX(), e.getY());
                if (child != null && mGestureDetector.onTouchEvent(e)) {
                    mDrawerLayout.closeDrawers();
                    if (context instanceof MainActivity && CLASSES[rv.getChildPosition(child)] != MainActivity.class) {
                        context.startActivity(new Intent(context, CLASSES[rv.getChildPosition(child)]));
                    }
                    if (!(context instanceof MainActivity) && CLASSES[rv.getChildPosition(child)] == MainActivity.class) {
                        context.finish();
                    }
                    if (!(context instanceof MainActivity) && CLASSES[rv.getChildPosition(child)] != MainActivity.class) {
                        context.startActivity(new Intent(context, CLASSES[rv.getChildPosition(child)]));
                        context.finish();
                    }
                    return true;
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }
        });
    }

    public static String capitalise(String string) {
        if (string == null || string.length() == 0) {
            return "";
        }
        return Character.toUpperCase(string.charAt(0)) + string.substring(1);
    }

    public int uploadFile(final Context context, String route, File sourceFile) {
        int serverResponseCode = 0;
        HttpURLConnection conn;
        DataOutputStream dos;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1024 * 1024;
        if (!sourceFile.isFile()) {
            Log.e("uploadFile", "Source File not exist: " + sourceFile.getAbsolutePath());
            return 0;
        } else {
            try {
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(route);
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setUseCaches(false);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("uploaded_file", sourceFile.getName());
                dos = new DataOutputStream(conn.getOutputStream());
                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\"" + sourceFile.getName() + "\"" + lineEnd);
                dos.writeBytes(lineEnd);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                while (bytesRead > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();
                Log.i("uploadFile", "HTTP Response is: " + serverResponseMessage + ": " + serverResponseCode);
                if (serverResponseCode == 200) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(context, "File Upload Complete.", Toast.LENGTH_LONG).show();
                        }
                    });
                }
                fileInputStream.close();
                dos.flush();
                dos.close();
            } catch (MalformedURLException ex) {
                ex.printStackTrace();
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(context, "MalformedURLException", Toast.LENGTH_LONG).show();
                    }
                });
                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
            } catch (Exception e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(context, "Got Exception: see logcat ", Toast.LENGTH_LONG).show();
                    }
                });
                Log.e("Upload file to server Exception", "Exception: " + e.getMessage(), e);
            }
            return serverResponseCode;
        }
    }
}
