package com.elmenus.elmenuscollector.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;

import com.elmenus.elmenuscollector.R;
import com.elmenus.elmenuscollector.adapters.ImagesDynamicGridViewAdapter;
import com.elmenus.elmenuscollector.helpers.ImageItem;

import org.askerov.dynamicgrid.DynamicGridView;

import java.io.File;
import java.util.ArrayList;

public class NewImagesActivity extends BaseActivity {

    ArrayList<ImageItem> images;

    DynamicGridView grid;
    ImageView deleteImage;
    Button saveButton;

    public static final int REQUEST_CODE_CAMERA = 8;
    public static final int REQUEST_CODE_GALLERY = 9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_images);
        initToolbar(this);

        grid = (DynamicGridView) findViewById(R.id.grid);
        deleteImage = (ImageView) findViewById(R.id.delete_image);
        grid.setDeleteImage(deleteImage);
        saveButton = (Button) findViewById(R.id.button_save);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("result", images);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        images = getIntent().getParcelableArrayListExtra("result");
        if (images == null) {
            images = new ArrayList<>();
        }

        grid.setAdapter(new ImagesDynamicGridViewAdapter(this, R.layout.layout_images_dynamic_gridview_adapter, images));
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(NewImagesActivity.this, ImageActivity.class);
                intent.putExtra("image", (Parcelable) images.get(position));
                startActivity(intent);
            }
        });
        grid.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                grid.startEditMode(position);
                deleteImage.setVisibility(View.VISIBLE);
                return true;
            }
        });
        grid.setOnDropListener(new DynamicGridView.OnDropListener() {
            @Override
            public void onActionDrop() {
                grid.stopEditMode();
                deleteImage.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_new_images, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_capture) {
            dispatchTakePictureIntent();
            return true;
        }
        if (id == R.id.action_choose) {
            dispatchChoosePictureIntent();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void dispatchTakePictureIntent() {
        Intent intent = new Intent(NewImagesActivity.this, CameraActivity.class);
        startActivityForResult(intent, REQUEST_CODE_CAMERA);
    }

    private void dispatchChoosePictureIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CODE_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_CAMERA && resultCode == RESULT_OK) {
            final ImageItem item = new ImageItem(CameraActivity.mFile);
            item.setRotation(90);
            images.add(item);
            ((ImagesDynamicGridViewAdapter) grid.getAdapter()).add(item);
        }
        else if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK) {
            Uri tempUri = data.getData();
            File file = new File(getRealPathFromURI(this, tempUri));
            final ImageItem item = new ImageItem(file);
            images.add(item);
            ((ImagesDynamicGridViewAdapter) grid.getAdapter()).add(item);
        }
    }
}
