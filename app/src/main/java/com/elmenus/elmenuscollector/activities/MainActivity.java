package com.elmenus.elmenuscollector.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.elmenus.elmenuscollector.R;
import com.elmenus.elmenuscollector.helpers.Constants;
import com.elmenus.elmenuscollector.network.GetCityRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends BaseActivity {

    TextView cityText;
    Button availableRestaurants, myCollections, myProfile, help;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initNavigationDrawer(this);

        cityText = (TextView) findViewById(R.id.text_city);
        GetCityRequest request = new GetCityRequest(Constants.currentUser.getCityId());
        newLoadingDialog(this).show();
        JsonObjectRequest volleyRequest = new JsonObjectRequest(Request.Method.GET,
                request.getQueryString(), request.getJsonRequest(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    cityText.setText(response.getJSONObject("data").getString("name_en"));
                    hideLoadingDialog();
                } catch (JSONException e) {
                    Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    hideLoadingDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                hideLoadingDialog();
            }
        });
        RequestQueue mRequestQueue = Volley.newRequestQueue(MainActivity.this);
        mRequestQueue.add(volleyRequest);
        availableRestaurants = (Button) findViewById(R.id.button_available_restaurants);
        availableRestaurants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AvailableRestaurantsActivity.class));
            }
        });
        myCollections = (Button) findViewById(R.id.button_my_collections);
        myCollections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, MyCollectionsActivity.class));
            }
        });
        myProfile = (Button) findViewById(R.id.button_my_profile);
        myProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, MyProfileActivity.class));
            }
        });
        help = (Button) findViewById(R.id.button_help);
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, HelpActivity.class));
            }
        });
    }
}