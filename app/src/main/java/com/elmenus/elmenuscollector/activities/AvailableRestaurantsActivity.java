package com.elmenus.elmenuscollector.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.elmenus.elmenuscollector.R;
import com.elmenus.elmenuscollector.adapters.AvailableRestaurantsListViewAdapter;
import com.elmenus.elmenuscollector.helpers.Constants;
import com.elmenus.elmenuscollector.helpers.InfiniteScrollListener;
import com.elmenus.elmenuscollector.helpers.LocationProvider;
import com.elmenus.elmenuscollector.models.Branch;
import com.elmenus.elmenuscollector.models.Restaurant;
import com.elmenus.elmenuscollector.network.GetRestaurantsSearchRequest;
import com.getbase.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AvailableRestaurantsActivity extends BaseActivity {

    private ArrayList<Restaurant> items;
    ListView mRestaurants;
    FloatingActionButton newRestaurant;

    EditText searchEditText;
    CheckBox nearby;
    CheckBox availableRestaurants;

    public static final int REQUEST_CODE_RESTAURANT = 13;

    LocationProvider provider;
    Location location;

    Button myCollectionsButton;
    Button mySubmissionsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_available_restaurants);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        initNavigationDrawer(this);

        myCollectionsButton = (Button) findViewById(R.id.button_my_collections);
        myCollectionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AvailableRestaurantsActivity.this, MyCollectionsActivity.class));
                finish();
            }
        });
        mySubmissionsButton = (Button) findViewById(R.id.button_my_submissions);
        mySubmissionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AvailableRestaurantsActivity.this, MySubmissionsActivity.class));
                finish();
            }
        });

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            new AlertDialog.Builder(this).setMessage("Please enable your GPS").setCancelable(false)
                    .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }).create().show();
            return;
        }

        provider = new LocationProvider(this);
        location = provider.getLocation();

        newLoadingDialog(this).show();

        mRestaurants = (ListView) findViewById(R.id.restaurants);

        searchEditText = (EditText) findViewById(R.id.edit_text_search);
        availableRestaurants = (CheckBox) findViewById(R.id.checkbox_available_restaurants);
        availableRestaurants.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                newLoadingDialog(AvailableRestaurantsActivity.this).show();
                getAvailableRestaurants(0, isChecked, nearby.isChecked(), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            ((AvailableRestaurantsListViewAdapter) mRestaurants.getAdapter()).clear();
                            ((AvailableRestaurantsListViewAdapter) mRestaurants.getAdapter()).addAll(Restaurant.parseRestaurantsJsonArray(response.getJSONObject("data").getJSONArray("results")));
                            hideLoadingDialog();
                        } catch (JSONException e) {
                            Toast.makeText(AvailableRestaurantsActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            hideLoadingDialog();
                        }
                    }
                });
            }
        });

        nearby = (CheckBox) findViewById(R.id.checkbox_nearby);
        nearby.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                newLoadingDialog(AvailableRestaurantsActivity.this).show();
                getAvailableRestaurants(0, availableRestaurants.isChecked(), isChecked, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            ((AvailableRestaurantsListViewAdapter) mRestaurants.getAdapter()).clear();
                            ((AvailableRestaurantsListViewAdapter) mRestaurants.getAdapter()).addAll(Restaurant.parseRestaurantsJsonArray(response.getJSONObject("data").getJSONArray("results")));
                            hideLoadingDialog();
                        } catch (JSONException e) {
                            Toast.makeText(AvailableRestaurantsActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            hideLoadingDialog();
                        }
                    }
                });
            }
        });

        items = new ArrayList<>();
        getAvailableRestaurants(0, availableRestaurants.isChecked(), nearby.isChecked(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    items.addAll(Restaurant.parseRestaurantsJsonArray(response.getJSONObject("data").getJSONArray("results")));
                    mRestaurants.setAdapter(new AvailableRestaurantsListViewAdapter(AvailableRestaurantsActivity.this, R.layout.layout_available_restaurants_listview_adapter, items));
                    searchEditText.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(final Editable s) {
                            setProgressBarIndeterminateVisibility(true);
                            getAvailableRestaurants(0, availableRestaurants.isChecked(),
                                    nearby.isChecked(), new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        ((AvailableRestaurantsListViewAdapter) mRestaurants.getAdapter()).clear();
                                        ((AvailableRestaurantsListViewAdapter) mRestaurants.getAdapter()).addAll(Restaurant.parseRestaurantsJsonArray(response.getJSONObject("data").getJSONArray("results")));
                                        setProgressBarIndeterminateVisibility(false);
                                    } catch (JSONException e) {
                                        Toast.makeText(AvailableRestaurantsActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                                        e.printStackTrace();
                                        setProgressBarIndeterminateVisibility(false);
                                    }
                                }
                            });
                        }
                    });
                    hideLoadingDialog();
                } catch (JSONException e) {
                    Toast.makeText(AvailableRestaurantsActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    hideLoadingDialog();
                }
            }
        });

        mRestaurants.setOnScrollListener(new InfiniteScrollListener(Constants.perPage) {
            @Override
            public void loadMore(final int page, int totalItemsCount) {
                setProgressBarIndeterminateVisibility(true);
                getAvailableRestaurants(page * Constants.perPage, availableRestaurants.isChecked(),
                        nearby.isChecked(), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            ((AvailableRestaurantsListViewAdapter) mRestaurants.getAdapter()).addAll(Restaurant.parseRestaurantsJsonArray(response.getJSONObject("data").getJSONArray("results")));
                            setProgressBarIndeterminateVisibility(false);
                        } catch (JSONException e) {
                            Toast.makeText(AvailableRestaurantsActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                            setProgressBarIndeterminateVisibility(false);
                        }
                    }
                });
            }
        });

        newRestaurant = (FloatingActionButton) findViewById(R.id.button_new_restaurant);
        newRestaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AvailableRestaurantsActivity.this, NewRestaurantActivity.class);
                intent.setAction("add");
                startActivityForResult(intent, REQUEST_CODE_RESTAURANT);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_available_restaurants, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_view_on_map) {
            Intent intent = new Intent(this, MapActivity.class);
            ArrayList<Restaurant> selectedRestaurants = ((AvailableRestaurantsListViewAdapter) mRestaurants.getAdapter()).getSelectedItems();
            if (selectedRestaurants.size() == 0) {
                Toast.makeText(AvailableRestaurantsActivity.this, "Please choose at least 1 restaurant", Toast.LENGTH_LONG).show();
                return false;
            }
            ArrayList<Location> locations = new ArrayList<>();
            for (Restaurant restaurant : selectedRestaurants) {
                for (Branch branch : restaurant.getBranches()) {
                    Location location = new Location(restaurant.toString() + " - " + branch.getArea().toString());
                    location.setLatitude(branch.getLatitude());
                    location.setLongitude(branch.getLongitude());
                    locations.add(location);
                }
            }
            intent.putParcelableArrayListExtra("result", locations);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_RESTAURANT && resultCode == RESULT_OK) {
            Constants.collections.add((Restaurant) data.getParcelableExtra("result"));
            saveCollections();
            startActivity(new Intent(this, MyCollectionsActivity.class));
            finish();
        }
    }

    private void getAvailableRestaurants(int offset, boolean availableOnly, boolean nearby, Response.Listener<JSONObject> response) {
        GetRestaurantsSearchRequest request = new GetRestaurantsSearchRequest();
        request.addParameter(GetRestaurantsSearchRequest.PARAM_QUERY, String.valueOf(searchEditText.getText().toString()));
        request.addParameter(GetRestaurantsSearchRequest.PARAM_CITY_ID, String.valueOf(Constants.currentUser.getCityId()));
        request.addParameter(GetRestaurantsSearchRequest.PARAM_OFFSET, String.valueOf(offset));
        request.addParameter(GetRestaurantsSearchRequest.PARAM_LIMIT, String.valueOf(Constants.perPage));
        location = provider.getLocation();
        if (location != null && nearby) {
            request.addParameter(GetRestaurantsSearchRequest.PARAM_LATITUDE, String.valueOf(location.getLatitude()));
            request.addParameter(GetRestaurantsSearchRequest.PARAM_LONGITUDE, String.valueOf(location.getLongitude()));
            request.addParameter(GetRestaurantsSearchRequest.PARAM_SORT, "nearby");
        }
        if (availableOnly) {
            request.addParameter(GetRestaurantsSearchRequest.PARAM_ASSIGNED, String.valueOf(Constants.currentUser.getId()));
        }
        JsonObjectRequest volleyRequest = new JsonObjectRequest(Request.Method.GET,
                request.getQueryString(), request.getJsonRequest(), response, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(AvailableRestaurantsActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                hideLoadingDialog();
            }
        });
        final RequestQueue mRequestQueue = Volley.newRequestQueue(this);
        mRequestQueue.add(volleyRequest);
    }
}
