package com.elmenus.elmenuscollector.activities;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.elmenus.elmenuscollector.R;
import com.elmenus.elmenuscollector.helpers.Constants;
import com.elmenus.elmenuscollector.network.GetPaymentSummaryRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MyPaymentsActivity extends BaseActivity {

    TextView[] textViews;
    Button historyButton;

    private static final int FACTOR = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_payments);
        initNavigationDrawer(this);

        textViews = new TextView[9];

        textViews[0] = (TextView) findViewById(R.id.received_restaurants);
        textViews[1] = (TextView) findViewById(R.id.received_total);
        textViews[2] = (TextView) findViewById(R.id.received_date);

        textViews[3] = (TextView) findViewById(R.id.pending_restaurants);
        textViews[4] = (TextView) findViewById(R.id.pending_branches);
        textViews[5] = (TextView) findViewById(R.id.pending_total);

        textViews[6] = (TextView) findViewById(R.id.approved_restaurants);
        textViews[7] = (TextView) findViewById(R.id.approved_branches);
        textViews[8] = (TextView) findViewById(R.id.approved_total);

        historyButton = (Button) findViewById(R.id.button_history);
        historyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetPaymentSummaryRequest request = new GetPaymentSummaryRequest();
                request.addParameter(GetPaymentSummaryRequest.PARAM_USER_ID, String.valueOf(Constants.currentUser.getId()));
                newLoadingDialog(MyPaymentsActivity.this).show();
                JsonObjectRequest volleyRequest = new JsonObjectRequest(Request.Method.GET,
                        request.getQueryString(), request.getJsonRequest(), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println(response);
                        try {
                            JSONArray responseArray = response.getJSONArray("data");
                            String result = "";
                            for (int i = 0; i < responseArray.length(); ++i) {
                                result += "ID: " + responseArray.getJSONObject(i).getString("restaurant_id") + "\n"
                                        + "Amount: " + calculateAmount(responseArray.getJSONObject(i).getString("points"));
                            }
                            new AlertDialog.Builder(MyPaymentsActivity.this).setTitle("History")
                                    .setMessage(result)
                                    .setNeutralButton("OK", null);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        hideLoadingDialog();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MyPaymentsActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                        hideLoadingDialog();
                    }
                });
                RequestQueue mRequestQueue = Volley.newRequestQueue(MyPaymentsActivity.this);
                mRequestQueue.add(volleyRequest);
            }
        });

        GetPaymentSummaryRequest request = new GetPaymentSummaryRequest();
        request.addParameter(GetPaymentSummaryRequest.PARAM_USER_ID, String.valueOf(Constants.currentUser.getId()));
        newLoadingDialog(this).show();
        JsonObjectRequest volleyRequest = new JsonObjectRequest(Request.Method.GET,
                request.getQueryString(), request.getJsonRequest(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println(response);
                try {
                    JSONArray responseArray = response.getJSONArray("data");
                    for (int i = 0; i < responseArray.length(); ++i) {
                        String decision = responseArray.getJSONObject(i).getString("decision");
                        String count = responseArray.getJSONObject(i).getString("restaurant_count");
                        String amount = calculateAmount(responseArray.getJSONObject(i).getString("points"));
                        String branches;
                        switch (decision) {
                            case "received":
                                String date = responseArray.getJSONObject(i).optString("date");
                                textViews[0].setText(textViews[0].getText() + count);
                                textViews[1].setText(textViews[1].getText() + amount);
                                textViews[2].setText(textViews[2].getText() + date);
                                break;
                            case "pending":
                                branches = responseArray.getJSONObject(i).optString("branch_count");
                                textViews[3].setText(textViews[3].getText() + count);
                                textViews[4].setText(textViews[4].getText() + branches);
                                textViews[5].setText(textViews[5].getText() + amount);
                                break;
                            case "approved":
                                branches = responseArray.getJSONObject(i).optString("branch_count");
                                textViews[6].setText(textViews[6].getText() + count);
                                textViews[7].setText(textViews[7].getText() + branches);
                                textViews[8].setText(textViews[8].getText() + amount);
                                break;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                hideLoadingDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MyPaymentsActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                hideLoadingDialog();
            }
        });
        RequestQueue mRequestQueue = Volley.newRequestQueue(this);
        mRequestQueue.add(volleyRequest);
    }

    private String calculateAmount(String points) {
        return String.valueOf(FACTOR * Long.parseLong(points));
    }
}
