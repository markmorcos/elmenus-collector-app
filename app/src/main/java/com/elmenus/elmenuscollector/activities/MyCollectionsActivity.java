package com.elmenus.elmenuscollector.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;

import com.elmenus.elmenuscollector.R;
import com.elmenus.elmenuscollector.adapters.MyCollectionsListViewAdapter;
import com.elmenus.elmenuscollector.helpers.Constants;
import com.elmenus.elmenuscollector.models.Restaurant;

import java.util.ArrayList;

public class MyCollectionsActivity extends BaseActivity {

    CheckBox availableCollections;
    ListView mRestaurants;

    public static final int REQUEST_CODE_RESTAURANT = 7;

    public ArrayList<Restaurant> temp = new ArrayList<>();

    Button availableRestaurantsButton;
    Button mySubmissionsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_collections);
        initNavigationDrawer(this);

        availableRestaurantsButton = (Button) findViewById(R.id.button_available_restaurants);
        availableRestaurantsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyCollectionsActivity.this, AvailableRestaurantsActivity.class));
                finish();
            }
        });
        mySubmissionsButton = (Button) findViewById(R.id.button_my_submissions);
        mySubmissionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyCollectionsActivity.this, MySubmissionsActivity.class));
                finish();
            }
        });

        String action = getIntent().getAction();
        if (action != null && action.equals("edit")) {
            Intent intent = new Intent(this, NewRestaurantActivity.class);
            intent.setAction(getIntent().getAction());
            Restaurant restaurant = getIntent().getParcelableExtra("result");
            Constants.collections.add(restaurant);
            saveCollections();
            intent.putExtra("position", Constants.collections.size() - 1);
            startActivityForResult(intent, REQUEST_CODE_RESTAURANT);
        }

        mRestaurants = (ListView) findViewById(R.id.restaurants);
        mRestaurants.setAdapter(new MyCollectionsListViewAdapter(this, R.layout.layout_my_collections_listview_adapter, Constants.collections));

        availableCollections = (CheckBox) findViewById(R.id.checkbox_available_collections);
        availableCollections.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    startActivity(new Intent(MyCollectionsActivity.this, MySubmissionsActivity.class));
                    finish();
                }
                /*
                if (isChecked) {
                    temp.clear();
                    temp.addAll(Constants.collections);
                    for (Restaurant restaurant : Constants.collections) {
                        if (!restaurant.getStatus().equals("started")) {
                            Constants.collections.remove(restaurant);
                        }
                    }
                    ((MyCollectionsListViewAdapter) mRestaurants.getAdapter()).notifyDataSetChanged();
                } else {
                    Constants.collections.clear();
                    Constants.collections.addAll(temp);
                    ((MyCollectionsListViewAdapter) mRestaurants.getAdapter()).notifyDataSetChanged();
                }
                */
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_RESTAURANT && resultCode == RESULT_OK) {
            String action = data.getAction();
            if (action != null && action.equals("edit")) {
                Restaurant restaurant = data.getParcelableExtra("result");
                int position = data.getIntExtra("position", -1);
                if (position != -1) {
                    Constants.collections.set(position, restaurant);
                    ((MyCollectionsListViewAdapter) mRestaurants.getAdapter()).notifyDataSetChanged();
                }
            }
            if (action != null && action.equals("submit")) {
                Restaurant restaurant = data.getParcelableExtra("result");
                int position = data.getIntExtra("position", -1);
                if (position != -1) {
                    Constants.collections.remove(position);
                    saveCollections();
                    ((MyCollectionsListViewAdapter) mRestaurants.getAdapter()).notifyDataSetChanged();
                    startActivity(new Intent(MyCollectionsActivity.this, MySubmissionsActivity.class));
                    finish();
                }
            }
            mRestaurants.requestLayout();
            saveCollections();
        }
    }
}
