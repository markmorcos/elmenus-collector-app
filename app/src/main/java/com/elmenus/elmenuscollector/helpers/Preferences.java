package com.elmenus.elmenuscollector.helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by anawara on 5/20/14.
 */
public class Preferences {

    public static final String KEY_USER_ACCESSTOKEN = "elm_acc_token";
    public static final String KEY_USER_NAME = "user_name";
    public static final String KEY_USER_PHOTO = "user_photo";
    public static final String KEY_USER_EMAIL = "user_email";
    public static final String KEY_USER_PASSWORD = "user_password";
    public static final String KEY_FB_AUTOSHARE = "fbautoshare";
    public static final String KEY_FB_AUTOSHARE_ASK = "facebook_autoshare_question";
    public static final String KEY_USER_ID = "userid";
    public static final String KEY_DEVICE_TOKEN = "token";
    public static final String KEY_CONF_THUMB= "conf_thumb";
    public static final String KEY_CONF_NORMAL= "conf_normal";
    public static final String KEY_AUTODETECT_LOCATION = "detect_location";
    public static final String PROPERTY_APP_VERSION = "appVersion";

    private Preferences() {
        throw new RuntimeException("You must use static methods!");
    }

    private static SharedPreferences elmenusPrefs;

    public static final SharedPreferences getPrefs(Context context) {
        if (context == null) {
            return null;
        }

        if (elmenusPrefs == null) {
            elmenusPrefs = context.getSharedPreferences(getPrefName(context), Context.MODE_PRIVATE);
        }

        return elmenusPrefs;
    }

    private static String getPrefName(Context context) {
        return "elmenus";
    }
}