package com.elmenus.elmenuscollector.helpers;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.ParseError;
import com.android.volley.MultiPartRequest;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by mark on 30/07/15.
 */
public class JSONMultipartRequest extends MultiPartRequest<JSONObject> {

    protected static final String PROTOCOL_CHARSET = "utf-8";

    public JSONMultipartRequest(int method, String url, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString =
                    new String(response.data, HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
            return Response.success(new JSONObject(jsonString),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }
}
