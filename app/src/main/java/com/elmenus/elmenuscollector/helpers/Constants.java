package com.elmenus.elmenuscollector.helpers;

import com.elmenus.elmenuscollector.models.Restaurant;
import com.elmenus.elmenuscollector.models.User;

import java.util.ArrayList;

/**
 * Created by mark on 04/07/15.
 */
public class Constants {
    public static final String URL_DEFAULT_IMAGE = "https://placeholdit.imgix.net/~text?txtsize=40&txt=Default&w=150&h=150";
    public static User currentUser;
    public static String accessToken;
    public static ArrayList<Restaurant> collections = new ArrayList<>();;
    public static int perPage = 50;
}
