package com.elmenus.elmenuscollector.helpers;

import android.os.Bundle;

import com.elmenus.elmenuscollector.models.Area;
import com.elmenus.elmenuscollector.models.Feature;
import com.elmenus.elmenuscollector.models.City;
import com.elmenus.elmenuscollector.models.Cuisine;
import com.elmenus.elmenuscollector.models.ReportType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by anawara on 5/25/14.
 */
public class Lookups {
    public static boolean WAS_APP_IN_BACKGROUND;
    public static int AREA_INDEX;
    public static int CITY_INDEX;
    public static Area CURRENT_AREA;
    public static Area AUTODETECTED_AREA;
    public static City CURRENT_CITY;
    public static long CITY_ID;
    public static ArrayList<City> CITIES;
    public static HashMap<Long, ArrayList<Area>> CITIES_MAP = new HashMap<Long, ArrayList<Area>>();
    public static ArrayList FEATURES;
    public static ArrayList CUISINES;
    public static boolean AUTODETECT_AREA = true;
    public static HashMap<Integer,ArrayList<ReportType>> REPORTS = new HashMap<Integer, ArrayList<ReportType>>();

    public static String[] getReports(int entityType){
        ArrayList<ReportType> reportObjects = REPORTS.get(entityType);
        String[] reports = new String[reportObjects.size()];
        for(int i=0; i<reports.length; i++){
            reports[i] = reportObjects.get(i).getNameEn();
        }
        return reports;
    }

    public static void reset(boolean resetCurrentArea){
        if(CITIES_MAP.get(CITY_ID)!=null)
            for(Area area: Lookups.CITIES_MAP.get(Lookups.CITY_ID)){
                if(!resetCurrentArea && area.getId() == Lookups.CURRENT_AREA.getId())
                    continue;
                area.setSelected(false);
            }

        if(Lookups.CUISINES!=null)
            for(int i=0; i< Lookups.CUISINES.size(); i++){
                ((Cuisine) Lookups.CUISINES.get(i)).setSelected(false);
            }

        if(Lookups.FEATURES!=null)
            for(int i=0; i< Lookups.FEATURES.size(); i++){
                ((Feature) Lookups.FEATURES.get(i)).setSelected(false);
            }
    }

    public static void savedInstanceState(Bundle b){
        Lookups.CITIES = b.getParcelableArrayList("CITIES");
        Lookups.CUISINES = b.getParcelableArrayList("CUISINES");
        Lookups.FEATURES = b.getParcelableArrayList("FEATURES");
        Lookups.CURRENT_CITY = b.getParcelable("CURRENT_CITY");
        Lookups.CURRENT_AREA = b.getParcelable("CURRENT_AREA");
        Lookups.AUTODETECTED_AREA = b.getParcelable("AUTODETECTED_AREA");
        Lookups.CITY_INDEX = b.getInt("CITY_INDEX");
        Lookups.CITY_ID = b.getLong("CITY_ID");
        Lookups.AREA_INDEX = b.getInt("AREA_INDEX");
        Lookups.AUTODETECT_AREA = b.getBoolean("AUTODETECTED");
        Configuration.NORMAL_BASE_URL = b.getString("CONFIG_NORMAL");
        Configuration.THUMBNAIL_BASE_URL = b.getString("CONFIG_THUMB");

        Lookups.CITIES_MAP = new HashMap<Long, ArrayList<Area>>();
        if (CITIES !=null)
            for(City city:CITIES){
                CITIES_MAP.put(city.getId(),city.getAreas());
            }

        ArrayList<ReportType> reports = b.getParcelableArrayList("REPORTS");
        if(reports!=null) {
            for (ReportType report : reports) {
                ArrayList<ReportType> entityReports = Lookups.REPORTS.get(report.getEntityType());
                if (entityReports == null) {
                    entityReports = new ArrayList<ReportType>();
                    Lookups.REPORTS.put(report.getEntityType(), entityReports);
                }
                entityReports.add(report);
            }
        }
    }

    public static void saveToBundle(Bundle outState){
        outState.putParcelableArrayList("CITIES", Lookups.CITIES);
        outState.putParcelableArrayList("CUISINES", Lookups.CUISINES);
        outState.putParcelableArrayList("FEATURES", Lookups.FEATURES);
        outState.putParcelable("CURRENT_CITY", Lookups.CURRENT_CITY);
        outState.putParcelable("CURRENT_AREA", Lookups.CURRENT_AREA);
        outState.putParcelable("AUTODETECTED_AREA", Lookups.AUTODETECTED_AREA);
        outState.putInt("CITY_INDEX", Lookups.CITY_INDEX);
        outState.putLong("CITY_ID", Lookups.CITY_ID);
        outState.putInt("AREA_INDEX", Lookups.AREA_INDEX);
        outState.putBoolean("AUTODETECTED", Lookups.AUTODETECT_AREA);
        outState.putString("CONFIG_NORMAL",Configuration.NORMAL_BASE_URL);
        outState.putString("CONFIG_THUMB",Configuration.THUMBNAIL_BASE_URL);

        ArrayList<ReportType> reports = new ArrayList<ReportType>();
        Iterator<Map.Entry<Integer,ArrayList<ReportType>>> iterator = REPORTS.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<Integer,ArrayList<ReportType>> entry = iterator.next();
            reports.addAll(entry.getValue());
        }

        outState.putParcelableArrayList("REPORTS", reports);

    }
}
