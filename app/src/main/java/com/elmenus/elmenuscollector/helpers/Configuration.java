package com.elmenus.elmenuscollector.helpers;

/**
 * Created by anawara on 5/20/14.
 */
public class Configuration {
    public static String THUMBNAIL_BASE_URL;
    public static String NORMAL_BASE_URL;
    public static String SEARCH_HEADER_BACKGROUND;
    public static String DINE_OUT_BACKGROUND;
    public static String DELIVERY_BACKGROUND;
}
